
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>
#include <stdio.h>

#define WIDTH 1024
#define HEIGHT 1024
#define SAMPLE(map, x, y) map[y * WIDTH + x]

int permutation[] = { 151,160,137,91,90,15,
    131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
    190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
    88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
    77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
    102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
    135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
    5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
    223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
    129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
    251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
    49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
    138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
};
int perm_size = 255;

struct Pixel {
	unsigned char r, g, b, a;
};

int interpolate(int a, int b, float t) {
	//printf("%f\n", t);
	int diff = b - a;
	float y = t * t * t * (t * (t * 6.0f - 15.0f) + 10.0f);
	return (int)(a + y * diff);
}

int main(int argc, char *argv[]) {

	for (int i = 0; i < perm_size; i++) {
		permutation[i] %= 255;
	}

	Pixel *image = (Pixel *)malloc(WIDTH * HEIGHT * sizeof(Pixel));
	int *grain = (int *)malloc(WIDTH * HEIGHT * sizeof(int));
	size_t size = WIDTH * HEIGHT * sizeof(int);
	//printf("%lu", size);
	float *additive = (float *)malloc(WIDTH * HEIGHT * sizeof(float));
	for (size_t x = 0; x < WIDTH; x++) {
		for (size_t y = 0; y < HEIGHT; y++) {
			SAMPLE(additive, x, y) = 0.0f;
			int value = permutation[(permutation[x % 255] + y) % 255];
			SAMPLE(grain, x, y) = value;
			//image[y * WIDTH + x].r = (unsigned char)value;
			//image[y * WIDTH + x].g = (unsigned char)value;
			//image[y * WIDTH + x].b = (unsigned char)value;
			//image[y * WIDTH + x].a = 255;
		}
	}

	int layer_count = 0;
	//size_t period = 32;
	float amplitude = 0.5f;
	for (size_t period = 256; period >= 2; period /= 2) {
		layer_count += 1;
		for (size_t x = 0; x < WIDTH; x++) {
			int sx1 = x / period * period;
			int sx2 = (sx1 + period) % WIDTH;
			for (size_t y = 0; y < HEIGHT; y++) {
				int sy1 = y / period * period;
				int sy2 = (sy1 + period) % HEIGHT;

				float horizontal_t = (float)(x - sx1) / (float)period;
				int value1 = interpolate(SAMPLE(grain, sx1, sy1), SAMPLE(grain, sx2, sy1), horizontal_t);
				int value2 = interpolate(SAMPLE(grain, sx1, sy2), SAMPLE(grain, sx2, sy2), horizontal_t);
				float vertical_t = (float)(y - sy1) / (float)period;
				SAMPLE(additive, x, y) += (float)(interpolate(value1, value2, vertical_t)) / 255.0f * amplitude;
				//SAMPLE(additive, x, y) = value1;
				 //= 0;//interpolate(value1, value2, t);
			}
		}
		amplitude *= 0.5f;
	}

	//float *i = 0;
	//*i = 30.0f;
	//
	printf("%d\n", layer_count);

	for (size_t x = 0; x < WIDTH; x++) {
		for (size_t y = 0; y < HEIGHT; y++) {
			
			unsigned char value = (unsigned char)(SAMPLE(additive, x, y) * 255.f);
			//printf("%d\n", (unsigned char)(SAMPLE(additive, x, y) / layer_count));
			SAMPLE(image, x, y).r = value;
			SAMPLE(image, x, y).g = value;
			SAMPLE(image, x, y).b = value;
			SAMPLE(image, x, y).a = 255;
			//image[y * WIDTH + x].r = (unsigned char)value;
			//image[y * WIDTH + x].g = (unsigned char)value;
			//image[y * WIDTH + x].b = (unsigned char)value;
			//image[y * WIDTH + x].a = 255;
			//uint
			//image.r = 
		}
	}

	stbi_write_png("noise.png",
		  WIDTH, HEIGHT, 4,
		  (void *)image,
		  0);

}
