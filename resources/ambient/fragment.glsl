#version 150

in vec2 f_uv;
out vec4 out_color;

uniform sampler2D g_diffuse;
uniform vec3 color;

void main(void) {
	out_color.rgb = texture(g_diffuse, f_uv).rgb * color;
	out_color.a = 1.0;
}
