#version 150

out vec4 out_color;
in vec3 f_normal;
in vec3 f_pos;
in vec4 f_light_pos;

uniform vec3 camera_loc;
uniform vec3 camera_dir;
uniform vec3 fog_color;

#define PL_COUNT 9
#define DL_COUNT 1

uniform vec3 pl_pos[PL_COUNT];
uniform vec3 pl_color[PL_COUNT];
uniform vec3 dl_direction[DL_COUNT];
uniform vec3 dl_color[DL_COUNT];
uniform sampler2D dl_shadow_map[DL_COUNT];

void calc_light(in vec3 world_view_dir,
		in vec3 world_light_dir,
		in vec3 world_normal,
		in float shininess,
		in float specular_strength,
		out float diffuse,
		out float specular)
{
	float world_cos_light = dot(-world_light_dir, world_normal);

	vec3 world_reflection_dir = normalize(world_light_dir + 2.0 * world_cos_light * world_normal);
	vec3 half_world_view_dir = normalize(world_light_dir + world_view_dir);

	diffuse = max(0.0, world_cos_light);
	specular = pow(max(0.0, dot(world_normal, -half_world_view_dir)), shininess) * specular_strength;
}

void main(void) {
	float max_distance = 20.0;
	float shininess = 1000.0;
	float specular_strength = 1.0;

	vec3 world_normal = normalize(f_normal);
	vec3 world_view_dir = normalize(f_pos - camera_loc);

	vec3 frag_color = vec3(0.8, 0.8, 0.5);
	out_color = vec4(0.0, 0.0, 0.0, 1.0);

	for (int i = 0; i < PL_COUNT; i++) {
		vec3 light_pos = pl_pos[i];
		vec3 light_color = pl_color[i];

		vec3 world_light_dir = normalize(f_pos - light_pos);
		float diffuse, specular;

		calc_light(world_view_dir, world_light_dir, world_normal, shininess, specular_strength, diffuse, specular);
		out_color.rgb += frag_color * (specular + diffuse) * light_color;
	}

	for (int i = 0; i < DL_COUNT; i++) {
		vec3 world_light_dir = normalize(dl_direction[i]);
		vec3 light_color = dl_color[i];
		float diffuse, specular;
		calc_light(world_view_dir, world_light_dir, world_normal, shininess, specular_strength, diffuse, specular);

		float bias = 0.005;
		vec3 proj_coords = f_light_pos.xyz / f_light_pos.w;
		proj_coords = proj_coords * 0.5 + 0.5;
		float depth = texture(dl_shadow_map[i], proj_coords.xy).r;
		float shadow = proj_coords.z - bias > depth ? 0.0 : 1.0;

		out_color.rgb += frag_color * (specular + diffuse) * light_color * shadow;
	}

	float dist = length(camera_loc - f_pos);
	float fog_weight = min(1.0, dist / max_distance);
	out_color.rgb = out_color.rgb * (1.0 - fog_weight) + fog_weight * fog_color;
}
