#version 150

in vec3 f_pos;

uniform vec3 light_pos;
uniform float far_plane;

void main(void) {
	gl_FragDepth = length(f_pos - light_pos) / far_plane;
	/*out_color = vec4(1.0, 1.0, 1.0, 1.0);*/
	/*gl_FragDepth = 0.0;*/
}
