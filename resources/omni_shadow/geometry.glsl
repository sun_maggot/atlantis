#version 150

layout (triangles) in;
layout (triangle_strip, max_vertices=18) out;

uniform mat4 surface_matrices[6];
out vec3 f_pos;

/*
 * note(adam):
 * Remember, this shader takes in primitives such as
 * vertices, or in this case, triangles. So what you 
 * are doing right now is actually putting the input
 * triangle onto all six faces of the cube map.
 *
 */

void main(void) {
	for (int i = 0; i < 6; i++) {
		gl_Layer = i;
		for (int j = 0; j < 3; j++) {
			f_pos = gl_in[j].gl_Position.xyz;
			gl_Position = surface_matrices[i] * vec4(f_pos, 1.0);
			EmitVertex();
		}
		EndPrimitive();
	}
}
