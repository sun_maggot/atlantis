#version 150

in vec2 f_uv;
out vec4 out_color;

uniform vec3 camera_pos;
uniform vec3 light_pos;
uniform sampler2D g_diffuse;
uniform sampler2D g_pos;
uniform sampler2D g_normal;

uniform vec3 light_color;
uniform samplerCubeShadow shadow_map;
uniform samplerCube shadow_map_debug;
uniform float far_plane;
uniform float radius;
uniform float intensity;

#define SAMPLE_INC 0.025
#define SAMPLE_MIN -0.05
#define SAMPLE_MAX 0.05

#define SAMPLE_SIZE 0.1
#define SAMPLE_COUNT 4

/*uniform float quadratic;*/
/*uniform float linear;*/
/*uniform float constant;*/

void main(void) {
	// Things to be uniforms
	const float shininess = 3000.0;
	const float specular_strength = 2.0;

	// Sampling g-buffer
	vec3 world_pos = texture(g_pos, f_uv).rgb;
	vec3 diffuse = texture(g_diffuse, f_uv).rgb;
	vec3 world_normal = texture(g_normal, f_uv).xyz;

	// Base lighting calculation
	vec3 world_light_dir = normalize(world_pos - light_pos);
	vec3 world_view_dir = normalize(world_pos - camera_pos);
	float world_cos_light = dot(-world_light_dir, world_normal);
	vec3 world_reflection_dir = normalize(world_light_dir + 2.0 * world_cos_light * world_normal);
	vec3 half_world_view_dir = normalize(world_light_dir + world_view_dir);
	float dif = max(0.0, world_cos_light);
	float spec = pow(max(0.0, dot(world_normal, -half_world_view_dir)), shininess) * specular_strength;
	vec3 color = diffuse * (dif + spec) * light_color;

	// Shadow calculations
	vec3 from_light = world_pos - light_pos;
	float cur_depth = length(from_light);

	// Dynamically vary he shadow elevation bias, so that surfaces tangent
	// to the light don't really get shadowed. It shouldn't matter with 
	// surfaces that are facing away from the light, since the light doesn't
	// really affect them anyway
	/*float sampled_depth = texture(shadow_map_debug, from_light).r * far_plane;*/
	/*float shadow_bias = 0.20 * (1.05 - dot(world_normal, -normalize(from_light)));*/
	/*float shadow_factor = texture(shadow_map, vec4(from_light, cur_depth / far_plane - shadow_bias / far_plane));*/
	/*float shadow_factor = texture(shadow_map, vec4(from_light, cur_depth / far_plane - shadow_bias / far_plane));*/
	float shadow_factor = 0.0;
	float sample_count = 0.0;

	
	float sample_start = -SAMPLE_SIZE * 0.5;
	float sample_inc = SAMPLE_SIZE / (SAMPLE_COUNT - 1);
	for (float i = 0; i < SAMPLE_COUNT; i+=1) {
		for (float j = 0; j < SAMPLE_COUNT; j+=1) {
			for (float k = 0; k < SAMPLE_COUNT; k+=1) {
				vec3 sample_offset = vec3(
						sample_start + i * sample_inc,
						sample_start + j * sample_inc,
						sample_start + k * sample_inc
					);
				vec3 from_light = world_pos + sample_offset - light_pos;
				float shadow_bias = 0.05;
				/*shadow_bias += 0.1 * (1.0 - dot(world_normal, -normalize(from_light)));*/
				shadow_bias += 0.05 * tan(acos(dot(world_normal, -normalize(from_light))));
				/*float shadow_bias = 0.05;*/
				shadow_factor += texture(shadow_map, vec4(from_light, cur_depth / far_plane - shadow_bias / far_plane));
			}
		}
	}
	shadow_factor /= SAMPLE_COUNT * SAMPLE_COUNT * SAMPLE_COUNT;
	


	/*for (int i = 0; i < 20; i++) {*/
		/*shadow_factor += texture(shadow_map, vec4(from_light + , cur_depth / far_plane - shadow_bias / far_plane));*/
	/*}*/

	// This is the old way we did things. Sampling shadow map direct and then
	// manually comparing the values.
	/*float shadow_factor = cur_depth - shadow_bias > shadow_depth ? 0.0 : 1.0;*/

	color *= shadow_factor;
	float dist = cur_depth;
	float rad_control = cos(3.1415926 * min(dist, radius) / radius) * 0.5 + 1;
	/*float attenuation = 1.0 / (cur_depth * cur_depth * quadratic + cur_depth * linear + constant);*/
	float attenuation = intensity / pow((dist / radius + 1), 2.0) * rad_control;
	/*float attenuation = intensity / dist * rad_control;*/
	color *= attenuation;

	out_color = vec4(color, 1.0);
	/*out_color = vec4(vec3(sampled_depth), 1.0);*/
}
