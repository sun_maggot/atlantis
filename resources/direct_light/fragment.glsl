#version 150

in vec2 f_uv;
out vec4 out_color;

uniform sampler2D g_diffuse;
uniform sampler2D g_pos;
uniform sampler2D g_normal;

uniform mat4 light_pv;
uniform vec3 camera_pos;
uniform vec3 light_dir;
uniform vec3 light_color;
uniform sampler2DShadow shadow_map;
uniform sampler2D shadow_map_debug;
/*uniform float far_plane;*/

void main(void) {
	// Things to be uniforms
	const float shininess = 3000.0;
	const float specular_strength = 2.0;

	// Sampling g-buffer
	vec3 world_pos = texture(g_pos, f_uv).rgb;
	vec3 diffuse = texture(g_diffuse, f_uv).rgb;
	vec3 world_normal = texture(g_normal, f_uv).xyz;

	// Base lighting calculation
	vec3 world_light_dir = normalize(light_dir);
	vec3 world_view_dir = normalize(world_pos - camera_pos);
	float world_cos_light = dot(-world_light_dir, world_normal);
	vec3 world_reflection_dir = normalize(world_light_dir + 2.0 * world_cos_light * world_normal);
	vec3 half_world_view_dir = normalize(world_light_dir + world_view_dir);
	float dif = max(0.0, world_cos_light);
	float spec = pow(max(0.0, dot(world_normal, -half_world_view_dir)), shininess) * specular_strength;
	vec3 color = diffuse * (dif + spec) * light_color;

	vec4 pos_relative_to_light = light_pv * vec4(world_pos, 1.0);
	pos_relative_to_light.xyz /= pos_relative_to_light.w;
	float cur_depth = pos_relative_to_light.z;
	{ // Shadow
		// Dynamically vary he shadow elevation bias, so that surfaces tangent
		// to the light don't really get shadowed. It shouldn't matter with 
		// surfaces that are facing away from the light, since the light doesn't
		// really affect them anyway
		/*float shadow_bias = 0.15 * (1.05 - dot(world_normal, -normalize(from_light)));*/
		float shadow_bias = 0.05;
		float shadow_factor = texture(shadow_map, vec3(pos_relative_to_light.xy * 0.5 + 0.5, (cur_depth - shadow_bias)));

		/*float debug_depth = texture(shadow_map_debug, vec2(pos_relative_to_light.xy * 0.5 + 0.5)).r;*/
		color *= shadow_factor;
		/*color = vec3(debug_depth);*/
		/*color = vec3(cur_depth);*/
		/*color = vec3(1, 0, 0);*/
	}

	/*{ // Attenuation
		float quadratic = 0.02, linear = 0.01, constant = 0.01;
		float attenuation = 1.0 / (cur_depth * cur_depth * quadratic + cur_depth * linear + constant);
		color *= attenuation;
	}

	// This is the old way we did things.
	/*float shadow_factor = cur_depth - shadow_bias > shadow_depth ? 0.0 : 1.0;*/

	out_color = vec4(color, 1.0);
}
