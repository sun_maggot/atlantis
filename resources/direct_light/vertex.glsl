#version 330

layout(location = 0)in vec3 pos;
layout(location = 2)in vec2 uv;

out vec2 f_uv;

void main(void) {
	gl_Position = vec4(pos, 1.0);
	f_uv = uv;
}
