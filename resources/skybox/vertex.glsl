#version 330

layout(location = 0)in vec3 pos;
/*layout(location = 2)in vec2 uv;*/

/*uniform vec3 look_dir;*/
uniform mat4 look_matrix;

out vec3 dir;

void main(void) {
	gl_Position = (look_matrix * vec4(pos, 1.0)).xyww;
	/*gl_Position.w = 1.0;*/
	/*gl_Position.z = 1.0;*/
	dir = pos;
}
