#version 150

out vec4 out_color;

uniform samplerCube tex;
in vec3 dir;

void main(void) {
	/*vec3 sample_dir = (look_matrix * vec4(look_dir, 1.0)).xyz;*/
	out_color = texture(tex, normalize(dir));
	/*gl_FragDepth = 1.0;*/
	/*out_color = vec4(1, 0,0, 1);*/
}
