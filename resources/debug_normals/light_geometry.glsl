#version 150

layout(points) in;
layout(line_strip, max_vertices = 2) out;

in vec3 v_normal[];
out vec4 v_color;

uniform mat4 world;
uniform mat4 projection;
uniform mat4 view;
uniform vec3 light_pos;
uniform vec3 camera_loc;
uniform vec3 camera_dir;

void main(void) {

	mat4 pv = projection * view;
	mat4 pvw = pv * world;
	vec4 world_pos = world * gl_in[0].gl_Position;
	vec4 world_normal = normalize(world * vec4(normalize(v_normal[0]), 0.0));

	vec4 world_light_dir = vec4(normalize(world_pos.xyz - light_pos.xyz), 0);

	float world_cos_light = dot(-world_light_dir, world_normal);

	vec4 world_reflection_dir = normalize(world_light_dir + 2.0 * world_cos_light * world_normal);
	world_reflection_dir *= max(0, dot(world_reflection_dir, world_normal));
	world_reflection_dir = normalize(world_reflection_dir);

	vec4 world_camera_to_vertex = vec4(normalize(world_pos.xyz - camera_loc), 0);

	vec4 pos = pv * world_pos;
	vec4 normal = pv * world_normal;
	vec4 light_dir = pv * world_light_dir;
	vec4 reflection_dir = pv * world_reflection_dir;
	vec4 camera_to_vertex = pv * world_camera_to_vertex;

	vec4 normal_color = vec4(1.0, 0.0, 0.0, 0.0);
	vec4 in_color = vec4(0.0, 1.0, 0.0, 0.0);
	vec4 out_color = vec4(1.0, 0.0, 1.0, 0.0);
	vec4 camera_dir_color = vec4(1.0, 1.0, 1.0, 1.0);

	v_color = normal_color;
	{
	v_color = normal_color;
		gl_Position = pos;
		EmitVertex();

	v_color = normal_color;
		gl_Position = pos + normal * 0.2;
		EmitVertex();
	}

	v_color = in_color;
	{
	v_color = in_color;
		gl_Position = pos;
		EmitVertex();

	v_color = in_color;
		gl_Position = pos - light_dir * 0.1;
		EmitVertex();
	}

	v_color = out_color;
	{
	v_color = out_color;
		gl_Position = pos;
		EmitVertex();

	v_color = out_color;
		gl_Position = pos + reflection_dir * 0.1;
		EmitVertex();
	}

	//vec4 camera_to_vertex = pvw * vec4(camera_dir, 0);
	v_color = camera_dir_color;
	{
		gl_Position = pos;
		EmitVertex();

		gl_Position = pos - camera_to_vertex * 0.8;
		EmitVertex();
	}


	EndPrimitive();
}
