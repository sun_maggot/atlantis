#version 150

out vec4 out_color;
in vec4 v_color;

void main(void) {
	out_color = v_color;
}
