#version 330

layout(location = 0)in vec3 pos;
layout(location = 1)in vec3 normal;
layout(location = 2)in vec2 uv;

out vec2 v_uv;
out vec3 v_normal;
out vec3 v_pos;

void main(void) {
	/*vec4 world_pos = world * vec4(pos, 1.0);*/
	/*gl_Position = projection * view * world_pos;*/
	/*v_uv = uv;*/
	/*v_normal = normal;*/
	/*v_normal = normalize((projection * view * world * vec4(normal, 0.0)).xyz);*/
	/*v_pos = world_pos.xyz;*/
	v_normal = normal;
	gl_Position = vec4(pos, 1.0);
}
