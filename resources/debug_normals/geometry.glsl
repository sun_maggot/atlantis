#version 150

layout(points) in;
layout(line_strip, max_vertices = 2) out;

in vec3 v_normal[];
out vec4 v_color;

uniform mat4 world;
uniform mat4 projection;
uniform mat4 view;
uniform vec3 light_pos;
uniform vec3 camera_loc;

void main(void) {

	mat4 pv = projection * view;
	mat4 pvw = pv * world;
	vec4 world_pos = world * gl_in[0].gl_Position;
	vec4 world_normal = normalize(world * vec4(normalize(v_normal[0]), 0.0));
	vec4 world_camera_to_vertex = vec4(normalize(world_pos.xyz - camera_loc), 0);

	vec4 pos = pv * world_pos;
	vec4 normal = pv * world_normal;
	vec4 normal_color = vec4(1.0, 0.0, 0.0, 0.0);

	v_color = normal_color;
	{
	v_color = normal_color;
		gl_Position = pos;
		EmitVertex();

	v_color = normal_color;
		gl_Position = pos + normal * 0.05;
		EmitVertex();
	}

	EndPrimitive();
}
