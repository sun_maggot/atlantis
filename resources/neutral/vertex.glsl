#version 330

layout(location = 0)in vec3 pos;
layout(location = 1)in vec3 normal;
layout(location = 2)in vec2 uv;

out vec2 f_uv;
out vec3 f_normal;
out vec3 f_pos;

uniform mat4 world;
uniform mat4 projection;
uniform mat4 view;

void main(void) {
	vec4 world_pos = world * vec4(pos, 1.0);
	gl_Position = projection * view * world_pos;

	f_uv = uv;
	f_normal = normalize((world * vec4(normal, 0.0)).xyz);
	f_pos = world_pos.xyz;
}
