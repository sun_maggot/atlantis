#version 330
in vec3 f_normal;
in vec3 f_pos;

layout(location = 0) out vec4 g_diffuse;
layout(location = 1) out vec3 g_normal;
layout(location = 2) out vec3 g_pos;

vec3 calc_diffuse() {
	/*return vec3(0.8, 0.8, 0.5);*/
	return vec3(0.8);
}

float calc_shininess() {
	return 1000.0;
}

float calc_specular() {
	return 1.0;
}

vec3 calc_normal() {
	vec3 world_normal = normalize(f_normal);
	return world_normal;
}

void main(void) {
	g_diffuse = vec4(calc_diffuse(), 1.0);
	g_normal = normalize(f_normal);
	g_pos = f_pos;
}

