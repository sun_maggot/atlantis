#version 150

in vec2 v_uv;
uniform sampler2D tex;

out vec4 out_color;

void main(void) {
	out_color = texture(tex, v_uv);
}
