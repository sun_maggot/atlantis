#version 330

/*
 * Only two shadow map buffers are needed, one for translucent fragments
 * and the other for opaque ones. If translucent fragments appear behind
 * opaque ones, it is discarded. If translucent fragments are compounded,
 * we simply create combined colours.
 *
 * Render the opaque map first, because if an opaque fragment gets in 
 * between two translucent ones, we cannot separate the inidividual trans-
 * lucent colours from the combined translucent reesult.
 */

layout(location = 0)in vec3 pos;

uniform mat4 world;
uniform mat4 light_pv;
/*uniform mat4 projection;*/
/*uniform mat4 view;*/

void main(void) {
	vec4 world_pos = world * vec4(pos, 1.0);
	/*gl_Position = projection * view * world_pos;*/
	gl_Position = light_pv * world * vec4(pos, 1.0);
}
