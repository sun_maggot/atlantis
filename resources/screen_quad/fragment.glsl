#version 150

uniform sampler2D tex;

in vec2 f_uv;
out vec4 out_color;

void main(void) {
	/*
	float depth = texture(tex, f_uv).r;
	out_color = vec4(depth, depth, depth, 1);
	*/
	out_color = texture(tex, f_uv);
	/*out_color.a = 1;*/
	/*out_color = vec4(1, 1, 0, 1);*/
}
