#version 330


layout(location = 0)in vec3 pos;
layout(location = 2)in vec2 uv;

uniform vec2 screen_size;
uniform vec2 screen_pos;

out vec2 f_uv;

void main(void) {
	vec3 end_pos = pos;
	end_pos.xy = (end_pos.xy + screen_pos) / (screen_size / 2);
	gl_Position = vec4(end_pos, 1.0);

	f_uv = uv;
}
