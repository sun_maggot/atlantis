# Atlantis #

![Alt text](https://media.giphy.com/media/xT4uQvYRq7Z0E3ciqs/giphy.gif)

Use WASD key to fly around and mouse to control camera.

This project is a playground to learn the past few decades of rendering techniques, as well as some different programming paradigms and memory management styles. Code is super messy because I'm trying a ton of stuff very quickly.

# Build #

## On windows:
Make sure visual studio 2015 is installed. Then go to the project folder and run:
```
setup.bat
build.bat
```

If you want to build with other versions of visual studio, you edit setup.bat to change the path. I can't guarantee that it'll still work though.


## On Mac:
Make sure XCode commandline tool is installed. Then run:
```
./build.sh
```