#include <stdio.h>
#include <stdlib.h>

#define array_size(array) (sizeof(array) / sizeof(array[0]))
#define internal static

#define b32 bool

enum TokenType {
	TokenType_NONE,

	TokenType_LBRACKET,
	TokenType_RBRACKET,
	TokenType_LBRACE,
	TokenType_RBRACE,
	TokenType_LPAREN,
	TokenType_RPAREN,
	TokenType_STRING,
	TokenType_FLOAT,
	TokenType_CHAR,
	TokenType_EOF,
	TokenType_COLON ,
	TokenType_SEMICOLON,
	TokenType_AMPERSAND,
	TokenType_ASTERISK,
	TokenType_NUMBER,
	TokenType_ID,
};

struct Token {
	TokenType type;
	char *start;
	int size;
};

bool is_end_of_line(char c) {
	return c == '\n'
		|| c == '\r'
	;
}

bool is_white_space(char c) {
	return is_end_of_line(c)
		|| c == '\t'
		|| c == ' '
		;
}

struct FileContent {
	char *text;
};

struct Tokenizer {
	char *at;
	TokenType state;
};

char *load_file_and_null_term(char *path) {
	FILE *handle = fopen(path, "r");
	if (!handle) {
		return 0;
	}
	fseek(handle, 0, SEEK_END);
	long int count = ftell(handle);
	printf("File size: %d\r\n", count);
	rewind(handle);

	char *text = (char *)malloc(sizeof(char) * (count));
	for (int i = 0; i < count; i++) {
		char c;
		if (fread(&c, 1, 1, handle) == 0) {
			text[i] = '\0';
			break;
		}
		text[i] = c;
	}
	fclose(handle);
	return text;
}

bool is_numeric(char c) {
	return (c >= '0' && c <= '9');
}

bool is_alphabet(char c) {
	return (c >= 'A' && c <= 'Z')
		|| (c >= 'a' && c <= 'z')
	;
}

bool is_id_character(char c) {
	return is_alphabet(c) || c == '_';
}

Token get_next_token(Tokenizer *t) {
	for (;;) {
		if (is_white_space(t->at[0])) {
			t->at++;
		}
		else if (t->at[0] == '/' && t->at[1] == '/') {
			t->at += 2;
			while (t->at[0] && !is_end_of_line(t->at[0])) {
				t->at++;
			}
			if (t->at[0]) {
				t->at++;
			}
		}
		else if (t->at[0] == '/' && t->at[1] == '*') {
			t->at += 2;
			while (t->at[0] && (t->at[0] != '*' || t->at[1] != '/')) {
				t->at++;
			}
			if (t->at[0]) {
				t->at += 2;
			}
		}
		else {
			break;
		}
	}

	Token token;
	token.type = TokenType_NONE;
	token.size = 1;
	token.start = t->at;
	switch (t->at[0]) {
		case '\0': { token.type = TokenType_EOF; } break;
		case '(': { token.type = TokenType_LPAREN; } break;
		case ')': { token.type = TokenType_LPAREN; } break;
		case '[': { token.type = TokenType_LBRACKET; } break;
		case ']': { token.type = TokenType_RBRACKET; } break;
		case '{': { token.type = TokenType_LBRACE; } break;
		case '}': { token.type = TokenType_RBRACE; } break;
		case ':': { token.type = TokenType_COLON; } break;
		case ';': { token.type = TokenType_SEMICOLON; } break;
		case '&': { token.type = TokenType_AMPERSAND; } break;
		case '*': { token.type = TokenType_ASTERISK; } break;
		case '"': { 
			t->at++;
			token.start = t->at;
			int size = 0;
			while (t->at[0] && t->at[0] != '"') {
				if (t->at[0] == '\\' &&
					t->at[1]) {
					size++;
					t->at++;
				}
				t->at++;
				size++;
			}
			if (t->at[0]) {
				token.type = TokenType_STRING;
				token.size = size;
			}
			else {
				t->at--;
			}
		} break;

		default: {
			if (is_id_character(t->at[0])) {
				t->at++;
				while (t->at[0] && (is_id_character(t->at[0]) || is_numeric(t->at[0]))) {
					t->at++;
					token.size++;
				}
				if (t->at[0]) {
					token.type = TokenType_ID;
				}
				else {
					token.type = TokenType_NONE;
					t->at--;
				}
			}
			else if (is_numeric(t->at[0])) {
				t->at++;
				bool had_point = false;
				while (t->at[0] &&
						(is_numeric(t->at[0])
						 || (t->at[0] == '.' && !had_point)
						 )
						) {
					if (t->at[0] == '.') { had_point = true; }
					token.size++;
					t->at++;
				}
				if (t->at[0]) {
					token.type = TokenType_NUMBER;
					if (t->at[0] == 'f') {
						token.size++;
						t->at++;
					}
				}
				else {
					token.type = TokenType_NONE;
					t->at--;
				}
			}
			else {
				token.type = TokenType_NONE;
			}
		}
	}

	t->at++;
	return token;
}

int main(int argc, char *argv[]) {

	FileContent content;

	char filename[] = "src/atlantis.h";
	content.text = load_file_and_null_term(filename);
	if (!content.text) {
		printf("Can't open file '%s'", filename);
		return 1;
	}

	Tokenizer tokenizer;
	tokenizer.at = content.text;
	tokenizer.state = TokenType_NONE;
	
	int token_capacity = 1000;
	int token_count = 0;
	Token *tokens = (Token *)malloc(sizeof(Token) * token_capacity);

	for(;;) {
		Token token = get_next_token(&tokenizer);
		if (token.type == TokenType_EOF) {
			break;
		}
		if (token.type != TokenType_NONE) {
			for (int i = 0; i < token.size; i++) {
				printf("%c", token.start[i]);
			}
			printf("\n");
		}
	}

	return 0;
}
