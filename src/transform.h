
#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "vector.h"
#include <ostream>
#include <iostream>
#include <cmath>
#include <iomanip>
template<int Height, int Width>
struct Matrix
{
	static_assert(Height > 0, "Can't really have 0 number or rows.");
	static_assert(Width > 0, "Can't really have 0 number or columns.");

	float m[Height * Width];

	static inline int size() {
		return Height * Width;
	}

	static inline int get_width() {
		return Width;
	}

	static inline int get_height() {
		return Height;
	}

	static inline int get_index(int row, int col) {
		return (row - 1) + Height * (col - 1);
	}

	inline float at(int row, int col = 1) const {
#ifdef DEBUG
		if (get_index(row, col) < 0 || get_index(row, col) >= Height * Width) {
			abort();
		}
#endif
		return m[get_index(row, col)];
	}

	inline float &at(int row, int col = 1) {
#ifdef DEBUG
		if (get_index(row, col) < 0 || get_index(row, col) >= Height * Width) {
			abort();
		}
#endif
		return m[get_index(row, col)];
	}

	inline float operator[](int i) const {
		return at(i);
	}

	inline float &operator[](int i) {
		return at(i);
	}

	static Matrix<Height, Width> empty() {
		Matrix<Height, Width> mat;
		for (int row = 1; row <= Height; row++) {
			for (int col = 1; col <= Width; col++) {
				mat.at(row, col) = 0.0f;
			}
		}
		return mat;
	}
};

template<int Height, int Width>
std::ostream &operator<<(std::ostream &out, const Matrix<Height, Width> &mat) {
	for (int row = 1; row <= Height; row++) {
		for (int col = 1; col <= Width; col++) {
			out << std::fixed << std::setw(10) << std::setprecision(6) << mat.at(row, col) << "\t";
		}
		out << std::endl;
	}
	return out;
}

template <int Size>
using SquareMatrix = Matrix<Size, Size>;

template<int Height1, int Width1, int Height2, int Width2>
inline Matrix<Height1, Width2> multiply(const Matrix<Height1, Width1>& a, const Matrix<Height2, Width2>& b) {
	static_assert(Width1 == Height2, "The number or columns in A must equal the number of rows in B");
	Matrix<Height1, Width2> c;
	for (int row = 1; row <= Height1; row++) {
		for (int col = 1; col <= Width2; col++) {
			c.at(row, col) = 0.0f;
			for (int i = 1; i <= Height2; i++) {
				c.at(row, col) += a.at(row, i) * b.at(i, col);
			}
		}
	}
	return c;
}

template <int Height, int Width>
inline Matrix<Width, Height> transpose(const Matrix<Height, Width> &mat) {
	Matrix<Width, Height> result;
	for (int col = 1; col <= Width; col++) {
		for (int row = 1; row <= Height; row++) {
			result.at(col, row) = mat.at(row, col);
		}
	}
	return result;
}

template<int Height1, int Width1, int Height2, int Width2>
inline Matrix<Height1, Width2> operator *(const Matrix<Height1, Width1> &a, const Matrix<Height2, Width2> &b) {
	return multiply(a, b);
}

template<int Size>
inline Matrix<Size, Size>& operator*=(Matrix<Size, Size> &self, const Matrix<Size, Size> &other) {
	self = self * other;
	return self;
}

template <int Size>
inline Vector<Size> multiply(const Matrix<Size, Size> &mat, const Vector<Size> &vec) {
	Vector<Size> result;
	for (int row = 1; row <= Size; row++) {
		result.at(row) = 0.0f;
		for (int col = 1; col <= Size; col++) {
			result.at(row) += mat.at(row, col) * vec.at(col);
		}
	}
	return result;
}

template<int Size>
inline Vector<Size> operator *(const Matrix<Size, Size> &mat, const Vector<Size> &vec) {
	return multiply(mat, vec);
}

template<int Size>
SquareMatrix<Size> identity_matrix() {
	SquareMatrix<Size> mat = SquareMatrix<Size>::empty();
	for (int i = 1; i <= Size; i++) {
		mat.at(i, i) = 1.0f;
	}
	return mat;
}

template<int Height, int Width>
Matrix<Height, Width * 2> concat(const Matrix<Height, Width>& a, const Matrix<Height, Width>& b) {
	Matrix<Height, Width * 2> c;
	for (int col = 1; col <= Width; col++) {
		for (int row = 1; row <= Height; row++) {
			c.at(row, col) = a.at(row, col);
			c.at(row, col + Width) = b.at(row, col);
		}
	}
	return c;
}

template<int Height, int Width>
void swap_rows(Matrix<Height, Width> *mat, int row1, int row2) {
	for (int col = 1; col <= Width; col++) {
		float value = mat->at(row1, col);
		mat->at(row1, col) = mat->at(row2, col);
		mat->at(row2, col) = value;
	}
}


template<int Size>
bool do_gauss(Matrix<Size, Size * 2> *mat) {

	for (int row = 1; row <= Size; row++) {

		// Find the row with the largest first elem,
		// including this row and below
		int max_row = row;
		{
			float max = std::abs(mat->at(row, row));
			for (int row_ = row + 1; row_ <= Size; row_++) {
				if (std::abs(mat->at(row_, row)) > max) {
					max = std::abs(mat->at(row_, row));
					max_row = row_;
				}
			}
		}

		// Swap with the rows below to get a bigger first memeber
		if (max_row != row) {
			swap_rows(mat, row, max_row);
		}

		// Not invertible, abort
		if (mat->at(row, row) == 0.0f) {
			return false;
		}

		{  // Divide the entire row by the first element
			float factor = 1.0f / mat->at(row, row);
			mat->at(row, row) = 1.0f;
			for (int col = row + 1; col <= Size * 2; col++) {
				mat->at(row, col) *= factor;
			}
		}

		// Eliminate the elements below of the same column
		for (int row_ = row + 1; row_ <= Size; row_++) {
			float factor = mat->at(row_, row);
			mat->at(row_, row) = 0.0f;
			for (int col = row + 1; col <= Size * 2; col++) {
				mat->at(row_, col) -= factor * mat->at(row, col);
			}
		}
	}

	// Eliminate all the other ones
	for (int row = 1; row <= Size; row++) {
		// Starting from one element right of diagonal
		for (int col = row + 1; col <= Size; col++) {
			float factor = mat->at(row, col);
			mat->at(row, col) = 0.0f;
			// Starting subtracting from one right from that
			for (int col_ = col + 1; col_ <= Size * 2; col_++) {
				mat->at(row, col_) -= factor * mat->at(col, col_);
			}
		}
	}

	return true;
}

template<int Size>
bool do_invert(SquareMatrix<Size> *mat) {
	Matrix<Size, Size * 2> concated = concat(*mat, identity_matrix<Size>());
	if (!do_gauss(&concated)) {
		return false;
	}
	SquareMatrix<Size> result;
	for (int row = 1; row <= Size; row++) {
		for (int col = 1; col <= Size; col++) {
			mat->at(row, col) = concated.at(row, col + Size);
		}
	}
	return true;
}

template<int Size>
SquareMatrix<Size> invert(const SquareMatrix<Size> &mat) {
	SquareMatrix<Size> result = mat;
	if (!do_invert(&result)) {
		return identity_matrix<Size>();
	}
	return result;
}

#endif //TRANSFORM_H

