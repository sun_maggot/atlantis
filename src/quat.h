
#ifndef QUAT_H
#define QUAT_H

#include "transform3D.h"
#include <cmath>

#ifndef M_PI
#define M_PI 3.1415926
#endif

struct Quat {
	float w, x, y, z;
	static inline Quat empty() {
		return { 1.0f, 0.0f, 0.0f, 0.0f };
	};
};

Quat normalize(const Quat& q);

inline Quat create_quat(const Vector3 &axis, float angle) {
	angle = angle;
	float half_rad = angle * M_PI / 360.0f;
	return normalize(Quat { 
		-std::cosf(half_rad),
		axis[1] * std::sinf(half_rad),
		axis[2] * std::sinf(half_rad),
		axis[3] * std::sinf(half_rad)
	});
}

inline Quat create_quat(const Vector3 &a, const Vector3 &b) {
	float angle = std::acos(dot_product(normalize(a), normalize(b))) * 180.0f / M_PI;
	Vector3 axis = normalize(a ^ b);
	return create_quat(axis, angle);
}

inline Quat x_quat(float angle) {
	return create_quat(create_vec3(1.0f, 0.0f, 0.0f), angle);
}

inline Quat y_quat(float angle) {
	return create_quat(create_vec3(0.0f, 1.0f, 0.0f), angle);
}

inline Quat z_quat(float angle) {
	return create_quat(create_vec3(0.0f, 0.0f, 1.0f), angle);
}

inline float length_squared(const Quat& q) {
	return q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w;
}

inline float length (const Quat& q) {
	return std::sqrt(length_squared(q));
}

inline Quat normalize(const Quat& q) {
	float len = length(q);
	return { q.w / len, q.x / len, q.y / len, q.z / len };
}

inline Quat operator*(const Quat& a, const Quat& b) {
	Quat result = Quat { 
		(a.w*b.w) - (a.x*b.x) - (a.y*b.y) - (a.z*b.z),
		(a.w*b.x) + (a.x*b.w) + (a.y*b.z) - (a.z*b.y),
		(a.w*b.y) - (a.x*b.z) + (a.y*b.w) + (a.z*b.x),
		(a.w*b.z) + (a.x*b.y) - (a.y*b.x) + (a.z*b.w)
	};
	return result;
}

inline Matrix4 to_matrix(const Quat& quat) {
	Quat q = normalize(quat);
	float x = q.x;
	float y = q.y;
	float z = q.z;
	float w = q.w;
	return Matrix4 {
		1.0f-2*y*y-2*z*z, 2*x*y+2*w*z, 2*x*z-2*w*y, 0.0f,
		2*x*y-2*w*z, 1.0f-2*x*x-2*z*z, 2*y*z+2*w*x, 0.0f,
		2*x*z+2*w*y, 2*y*z-2*w*x, 1.0f-2*x*x-2*y*y, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};
}

inline Vector4 operator*(const Quat& quat, const Vector4& vec) {
	return to_matrix(quat) * vec;
}

inline Matrix4 operator*(const Quat& quat, const Matrix4& mat) {
	return to_matrix(quat) * mat;
}

#endif //QUAT_H
