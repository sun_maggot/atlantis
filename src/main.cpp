#ifndef __APPLE__
#define GLEW_STATIC 
#include <GL/glew.h>
#else
#define GLFW_INCLUDE_GLCOREARB
#endif

#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>

#include "transform3D.h"
#include "quat.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "atlantis.h"

#define LIGHT_FAR_PLANE 15.0f
#define FIXED_LIGHT_FAR_PLANE 50.0f

#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1050
//#define SCREEN_WIDTH 1440
//#define SCREEN_HEIGHT 850
//#define SCREEN_WIDTH 640
//#define SCREEN_HEIGHT 480
//#define SHADOW_MAP_WIDTH 512
//#define SHADOW_MAP_HEIGHT 512
#define SHADOW_MAP_WIDTH 1024
#define SHADOW_MAP_HEIGHT 1024

using namespace std;

#include "mesh_generators.cpp"

struct {
	bool right = false;
	bool left = false;
	bool forward = false;
	bool backward = false;
	bool down = false;
	bool up = false;
	bool show_normal = false;
	bool draw_shadow_map = false;
	bool draw_lights = false;
	int gbuffer_draw = 0;
	struct {
		double last_x = 0.0f;
		double last_y = 0.0f;
		float dx = 0.0f, dy = 0.0f;
	} mouse;
			   
} inputs;

struct GL_StaticMesh {
	int index_count;
	int vertex_count;
	GLuint vao;
	GLuint vbo;
	GLuint ebo;
	size_t positions_offset, normals_offset, uv_offset;
	size_t vertex_buffer_size;
};

struct {
	Vector3 position = { 0.0f, 1.0f, 0.0f };
	struct {
		float x = 45.0f;
		float y = 0.0f;
	} rotation;
} camera;

struct PointLights {
	int count;
	Vector3 *positions;
	Vector3 *colors;
	bool *active;
	void *plat_data;
	bool *shadowing;	
	float *radii;
	float *intensities;
};

struct GL_PointLights {
	GLuint *shadow_fbos;
	GLuint *shadow_rbos;
	GLuint *shadow_textures;
};

struct DirectLights {
	int count;
	Vector3 *colors;
	Vector3 *directions;
	bool *casts_shadow;
	uint32 *widths;
	uint32 *heights;
	bool *active;
	bool *shadowing;	
	void *plat_data;
};

//struct GL_DirectLights {
	//GLuint *shadow_fbos;
	//GLuint *shadow_rbos;
	//GLuint *shadow_textures;
	//bool *has_map;
/*};*/

struct Shadowmap {
	uint32 width, height;
	void *plat_data;
};

struct GL_Shadowmap {
	GLuint texture;
	GLuint fbo;
};

struct Transform {
	Vector3 position = Vector3::empty();
	Quat rotation = Quat::empty();
	Vector3 scale = Vector3 { 1.0f, 1.0f, 1.0f };
};

template<int Size>
ostream &operator<<(ostream &out, const Vector<Size>& vec) {
	out << "vec(";
	out << vec.at(1);
	for (int i = 2; i <= Size; i++) {
		out << ", " << vec.at(i);
	}
	out << ")";
	return out;
}

struct ImageData {
	uint8 *pixels;
	int width;
	int height;
	int bpp;
};

bool load_image(const char *path, ImageData *img_data) {
	img_data->pixels = stbi_load(path, &img_data->width, &img_data->height, &img_data->bpp, 4);
	if (!img_data->pixels) {
		cout << "Could not load image '" << path << "'" << endl;
		return false;
	}
	return true;
}

bool load_string(const char *path, string *out_string) {

	ifstream t(path);
	if (!t) {
		cout << "Couldn not load file '" << path << "'." << endl;
		return false;
	}
	*out_string = string(std::istreambuf_iterator<char>(t), std::istreambuf_iterator<char>());
	return true;
}

bool load_shader(const char *path, GLuint shader_type, GLuint *shader) {
	*shader = glCreateShader(shader_type);
	string source;
	if (!load_string(path, &source)) {
		return false;
	}
	auto source_ptr = source.c_str();
	GLint length = source.size();
	glShaderSource(*shader, 1, &source_ptr, &length);
	glCompileShader(*shader);
	
	GLint compile_status;
	glGetShaderiv(*shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status == GL_FALSE) {
		cout << "Shader compilation '" << path << "' failed." << endl;
		GLint log_length;
		glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &log_length);

		char *error_message = (char *)malloc(sizeof(char) * (log_length + 1));
		glGetShaderInfoLog(*shader, log_length + 1, 0, error_message);
		cout << error_message << endl;

		free(error_message);
		return false;
	}
	else {
		cout << "Compilation of '" << path << "' was successful." << endl;
		return true;
	}
}

bool create_program(GLuint *shaders, int size, GLuint *program)
{
	*program = glCreateProgram();
	for (int i = 0; i < size; i ++) {
		glAttachShader(*program, shaders[i]);
	}
	glLinkProgram(*program);

	GLint link_status;
	glGetProgramiv(*program, GL_LINK_STATUS, &link_status);
	if (link_status == GL_FALSE) {
		cout << "Linking program failed." << endl;

		GLint log_length;
		glGetProgramiv(*program, GL_INFO_LOG_LENGTH, &log_length);

		char *err_msg = (char *)malloc(sizeof(char) * (log_length + 1));
		glGetProgramInfoLog(*program, log_length + 1, 0, err_msg);
		cout << err_msg << endl;
		free(err_msg);

		return false;
	}
	else {
		cout << "Linking program successful." << endl;
		return true;
	}
}

template <typename T>
T *advance(void **mem, int count) {
	T *start = (T *)*mem;
	*mem = (void*)(((T *)*mem) + count);
	return start;
}

template <typename T>
T *advance(void **mem, int count, T value) {
	T *result = advance<T>(mem, count);
	for (int i = 0; i < count; i++) {
		result[i] = value;
	}
	return result;
}

void init_platform_data(void **mem, StaticMesh *mesh);

StaticMesh *allocate_static_mesh(void **mem, int vertex_count, int index_count) {
	StaticMesh *mesh = advance<StaticMesh>(mem, 1);
	Vector3 *positions = advance<Vector3>(mem, vertex_count);
	Vector3 *normals = advance<Vector3>(mem, vertex_count);
	Vector2 *uv = advance<Vector2>(mem, vertex_count);
	uint32 *indices = advance<uint32>(mem, index_count);

	// offsets
	size_t offset = 0;
	mesh->positions_offset = offset;
	offset += sizeof(Vector3) * vertex_count;
	mesh->normals_offset = offset;
	offset += sizeof(Vector3) * vertex_count;
	mesh->uv_offset = offset;

	mesh->vertex_count = vertex_count;
	mesh->positions = positions;
	mesh->normals = normals;
	mesh->uv = uv;

	mesh->index_count = index_count;
	mesh->indices = indices;

	return mesh;
}

void calc_vertex_normals(StaticMesh *mesh) {
	for (int i = 0; i < mesh->vertex_count; i++) {
		mesh->normals[i] = {0.0f, 0.0f, 0.0f};
	}

	for (int i = 0; i < mesh->index_count; i += 3) {
		uint32 i1 = mesh->indices[i+0];
		uint32 i2 = mesh->indices[i+1];
		uint32 i3 = mesh->indices[i+2];

		Vector3 *v1 = &mesh->positions[i1];
		Vector3 *v2 = &mesh->positions[i2];
		Vector3 *v3 = &mesh->positions[i3];

		Vector3 normal = normalize((*v1 - *v2) ^ (*v3 - *v2));
		mesh->normals[i1] += normal;
		mesh->normals[i2] += normal;
		mesh->normals[i3] += normal;
	}

	for (int i = 0; i < mesh->vertex_count; i++) {
		mesh->normals[i] = normalize(mesh->normals[i]);
	}
}

void init_platform_data(void **mem, StaticMesh *mesh) {
	GL_StaticMesh *gl_mesh = advance<GL_StaticMesh>(mem, 1);
	gl_mesh->index_count = mesh->index_count;
	gl_mesh->vertex_count = mesh->vertex_count;

	// offsets
	gl_mesh->positions_offset = mesh->positions_offset;
	gl_mesh->normals_offset = mesh->normals_offset;
	gl_mesh->uv_offset = mesh->uv_offset;

	gl_mesh->vertex_buffer_size = (sizeof(Vector3) * 2 + sizeof(Vector2)) * mesh->vertex_count;

	glGenVertexArrays(1, &gl_mesh->vao);
	glBindVertexArray(gl_mesh->vao);

	{
		glGenBuffers(1, &gl_mesh->vbo);
		glBindBuffer(GL_ARRAY_BUFFER, gl_mesh->vbo);
		glBufferData(GL_ARRAY_BUFFER, gl_mesh->vertex_buffer_size, mesh->positions, GL_STATIC_DRAW);
	}

	{
		glGenBuffers(1, &gl_mesh->ebo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_mesh->ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * mesh->index_count, mesh->indices, GL_STATIC_DRAW);
	}

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)(gl_mesh->positions_offset));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)(gl_mesh->normals_offset));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)(gl_mesh->uv_offset));
	glEnableVertexAttribArray(2);

	glBindVertexArray(0);

	mesh->plat_data = (void *)gl_mesh;
}

void on_keydown(GLFWwindow *window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	if (key == GLFW_KEY_A) {
		if (action == GLFW_PRESS) {
			inputs.left = true;
		}
		else if (action == GLFW_RELEASE) {
			inputs.left = false;
		}
	}
	if (key == GLFW_KEY_D) {
		if (action == GLFW_PRESS) {
			inputs.right = true;
		}
		else if (action == GLFW_RELEASE) {
			inputs.right = false;
		}
	}
	if (key == GLFW_KEY_W) {
		if (action == GLFW_PRESS) {
			inputs.forward = true;
		}
		else if (action == GLFW_RELEASE) {
			inputs.forward = false;
		}
	}
	if (key == GLFW_KEY_S) {
		if (action == GLFW_PRESS) {
			inputs.backward = true;
		}
		else if (action == GLFW_RELEASE) {
			inputs.backward = false;
		}
	}
	if (key == GLFW_KEY_LEFT_SHIFT) {
		if (action == GLFW_PRESS) {
			inputs.down = true;
		}
		else if (action == GLFW_RELEASE) {
			inputs.down = false;
		}
	}
	if (key == GLFW_KEY_SPACE) {
		if (action == GLFW_PRESS) {
			inputs.up = true;
		}
		else if (action == GLFW_RELEASE) {
			inputs.up = false;
		}
	}
	if (key == GLFW_KEY_K) {
		if (action == GLFW_PRESS) {
			inputs.draw_shadow_map = !inputs.draw_shadow_map;
		}
	}
	if (key == GLFW_KEY_L) {
		if (action == GLFW_PRESS) {
			inputs.draw_lights = !inputs.draw_lights;
		}
	}

	if (key == GLFW_KEY_N && action == GLFW_PRESS) {
		inputs.show_normal = !inputs.show_normal;
	}

	if (key == GLFW_KEY_P && action == GLFW_PRESS) {
		inputs.gbuffer_draw++;
		if (inputs.gbuffer_draw > 3) {
			inputs.gbuffer_draw = 0;
		}
	}
}

//void init_platform_data(void **mem, DirectLights *lights) {
	//GL_DirectLights *data = advance<GL_DirectLights>(mem, 1);
	//data->shadow_fbos = advance<GLuint>(mem, lights->count);
	//data->shadow_rbos = advance<GLuint>(mem, lights->count);
	//data->shadow_textures = advance<GLuint>(mem, lights->count);
	//data->has_map = advance<bool>(mem, true);
	//lights->plat_data = (void*)data;

	//glGenFramebuffers(lights->count, data->shadow_fbos);
	//glGenRenderbuffers(lights->count, data->shadow_rbos);
	//glGenTextures(lights->count, data->shadow_textures);

	//for (int i = 0; i < lights->count; i++) {
		//glBindTexture(GL_TEXTURE_2D, data->shadow_textures[i]);
		//glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);  
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);  
		//glBindTexture(GL_TEXTURE_2D, 0);

		////glBindRenderbuffer(GL_RENDERBUFFER, data->shadow_rbos[i]); 
		////glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);  
		////glBindRenderbuffer(GL_RENDERBUFFER, 0);

		//glBindFramebuffer(GL_FRAMEBUFFER, data->shadow_fbos[i]);
		//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, data->shadow_textures[i], 0); 
		////glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, data->shadow_rbos[i]);
		//glDrawBuffer(GL_NONE);
		//glReadBuffer(GL_NONE);
		//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//}
/*}*/

PointLights *allocate_point_lights(void **mem, int count) {
	PointLights *lights = advance<PointLights>(mem, 1);
	lights->count = count;
	lights->positions = advance<Vector3>(mem, count, Vector3::empty());
	lights->colors = advance<Vector3>(mem, count, {1.0f, 1.0f, 1.0f});
	lights->active = advance<bool>(mem, count, false);
	lights->shadowing = advance<bool>(mem, count, false);
	lights->radii = advance<float>(mem, count, 50.0f);
	lights->intensities = advance<float>(mem, count, 1.0f);
	return lights;
}

DirectLights *allocate_direct_lights(void **mem, int count) {
	DirectLights *lights = advance<DirectLights>(mem, 1);
	lights->count = count;
	lights->colors = advance<Vector3>(mem, count, {1.0f, 1.0f, 1.0f});
	lights->directions = advance<Vector3>(mem, count, {0.0f, -1.0f, 0.0f});
	lights->casts_shadow = advance<bool>(mem, count, true);
	//lights->widths = advance<uint32>(mem, count, SHADOW_MAP_WIDTH);
	//lights->heights = advance<uint32>(mem, count, SHADOW_MAP_HEIGHT);
	lights->active = advance<bool>(mem, count, false);
	lights->shadowing = advance<bool>(mem, count, false);
	//init_platform_data(mem, lights);
	return lights;
}

bool log_gl_error() {
	switch(glGetError()) {
		case GL_NO_ERROR:
			return true;
		case GL_INVALID_ENUM:
			cout << "GL_INVALID_ENUM" << endl;
			break;
		case GL_INVALID_VALUE:
			cout << "GL_INVALID_VALUE" << endl;
			break;
		case GL_INVALID_OPERATION:
			cout << "GL_INVALID_OPERATION" << endl;
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			cout << "GL_INVALID_FRAMEBUFFER_OPERATION" << endl;
			break;
		case GL_OUT_OF_MEMORY:
			cout << "GL_OUT_OF_MEMORY" << endl;
			break;
		//case GL_STACK_UNDERFLOW:
			//cout << "GL_STACK_UNDERFLOW" << endl;
			//break;
		//case GL_STACK_OVERFLOW:
			//cout << "GL_STACK_OVERFLOW" << endl;
			//break;
	}
	return false;
}

void dump_gl_error() {
	while (!log_gl_error()) {}
}

void log_framebuffer() {
    switch(glCheckFramebufferStatus(GL_FRAMEBUFFER))
	{
		case GL_FRAMEBUFFER_COMPLETE:
			cout << "GL_FRAMEBUFFER_COMPLETE" << endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			cout << "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT" << endl;
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			cout << "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT" << endl;
			break;
	}
}

void init_omni_shadowmap_platform_data(void **mem, Shadowmap *map) {
	GL_Shadowmap *data = (GL_Shadowmap *)advance<GL_Shadowmap>(mem, 1);
	map->plat_data = (void *)data;

	glGenTextures(1, &data->texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, data->texture);
	//glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_DEPTH_COMPONENT, map->width, map->height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
		//while (!log_gl_error()) {}
	for (int i = 0; i < 6; i++) {
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, map->width, map->height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
		//cout << i << endl;
		//while (!log_gl_error()) {}
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	glGenFramebuffers(1, &data->fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, data->fbo);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, data->texture, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	log_framebuffer();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

Shadowmap *allocate_omni_shadowmap(void **mem, uint32 width, uint32 height) {
	Shadowmap *map = (Shadowmap *)advance<Shadowmap>(mem, 1);
	map->width = width;
	map->height = height;
	init_omni_shadowmap_platform_data(mem, map);
	return map;
};

void init_direct_shadowmap_platform_data(void **mem, Shadowmap *map) {
	GL_Shadowmap *data = (GL_Shadowmap *)advance<GL_Shadowmap>(mem, 1);
	map->plat_data = (void *)data;

	glGenTextures(1, &data->texture);
	glBindTexture(GL_TEXTURE_2D, data->texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, map->width, map->height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenFramebuffers(1, &data->fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, data->fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, data->texture, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	log_framebuffer();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

Shadowmap *allocate_direct_shadowmap(void **mem, uint32 width, uint32 height) {
	Shadowmap *map = (Shadowmap *)advance<Shadowmap>(mem, 1);
	map->width = width;
	map->height = height;
	init_direct_shadowmap_platform_data(mem, map);
	return map;
}

void calc_light_matrices(Vector3 *in_direction, Matrix4 *view, Matrix4 *projection) {
	*projection = ortho3D(18.0f, 18.0f, 0.1f, 25.0f);
	*view = identity3D();
	{
		Matrix4 iview = identity3D();
		Quat orientation = create_quat(*in_direction, create_vec3(0.0f, 0.0f, -1.0f));
		iview = to_matrix(orientation) * iview;
		iview = translate3D(5.0f, 15.0f, -5.0f) * iview;
		*view = invert(iview);
	}
}

void render_shadow_map(GLuint program, StaticMesh *s_mesh, Matrix4 *world, Matrix4 *view, Matrix4 *projection) {

	GL_StaticMesh *mesh = (GL_StaticMesh *)s_mesh->plat_data;
	glUseProgram(program);

	glBindVertexArray(mesh->vao);

	glUniformMatrix4fv(glGetUniformLocation(program, "world"), 1, GL_FALSE, (GLfloat *)world);
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, (GLfloat *)projection);
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, (GLfloat *)view);
	glDrawElements(GL_TRIANGLES, mesh->index_count, GL_UNSIGNED_INT, 0);
}

void render_screen_quad(GLuint program, StaticMesh *s_mesh, GLuint texture, float x, float y) {

	GL_StaticMesh *mesh = (GL_StaticMesh *)s_mesh->plat_data;
	glUseProgram(program);

	glBindVertexArray(mesh->vao);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUniform1i(glGetUniformLocation(program, "tex"), 0);

	glUniform2f(glGetUniformLocation(program, "screen_size"), SCREEN_WIDTH, SCREEN_HEIGHT);
	glUniform2f(glGetUniformLocation(program, "screen_pos"), x, y);

	glDrawElements(GL_TRIANGLES, mesh->index_count, GL_UNSIGNED_INT, 0);
}


//void render_untextured_static_mesh(GLuint program, PointLights *plights, DirectLights *dlights, Vector3 *camera_position, Vector3 *fog_color, StaticMesh *s_mesh, Matrix4 *world, Matrix4 *view, Matrix4 *projection) {
	//GL_StaticMesh *mesh = (GL_StaticMesh *)s_mesh->plat_data;
	//glUseProgram(program);
	//glBindVertexArray(mesh->vao);

	//int pl_count = 0;
	//int dl_count = 0;

	//auto print_uniform = [](GLuint program, const char *uniform) {
		//cout << uniform << ": " << glGetUniformLocation(program, uniform) << endl;
	//};

	//for (int i = 0; i < 1; i ++) {
		//glActiveTexture(GL_TEXTURE0);
		//GL_DirectLights *data = (GL_DirectLights *)dlights->plat_data;
		//glBindTexture(GL_TEXTURE_2D, data->shadow_textures[i]);
		//glUniform1i(glGetUniformLocation(program, "dl_shadow_map"), 0);

		//Matrix4 view, projection;
		//calc_light_matrices(&dlights->directions[0], &view, &projection);
		//Matrix4 light_matrix = projection * view;

		//glUniformMatrix4fv(glGetUniformLocation(program, "light_matrix"), 1, GL_FALSE, (GLfloat *)&light_matrix);
	//}

	//glUniform3fv(glGetUniformLocation(program, "pl_pos"), plights->count, (GLfloat *)plights->positions);
	//glUniform3fv(glGetUniformLocation(program, "pl_color"), plights->count, (GLfloat *)plights->colors);
	//glUniform3fv(glGetUniformLocation(program, "dl_direction"), dlights->count, (GLfloat *)dlights->directions);
	//glUniform3fv(glGetUniformLocation(program, "dl_color"), dlights->count, (GLfloat *)dlights->colors);

	//glUniform3f(glGetUniformLocation(program, "camera_loc"), (*camera_position)[1], (*camera_position)[2], (*camera_position)[3]);
	//glUniform3f(glGetUniformLocation(program, "fog_color"), (*fog_color)[1], (*fog_color)[2], (*fog_color)[3]);

	//glUniformMatrix4fv(glGetUniformLocation(program, "world"), 1, GL_FALSE, (GLfloat *)world);
	//glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, (GLfloat *)projection);
	//glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, (GLfloat *)view);

	//glDrawElements(GL_TRIANGLES, mesh->index_count, GL_UNSIGNED_INT, 0);
/*}*/

struct BoundingBox {
	Vector3 p1, p2;
};

BoundingBox get_bounding_box(StaticMesh *mesh) {

	// TODO(adam): Check for if it's empty
	float bias = 0.1f;
	float left, right, top, bottom, front, back;
	left = mesh->positions[0].x();
	right = mesh->positions[0].x();
	top = mesh->positions[0].y();
	bottom = mesh->positions[0].y();
	front = mesh->positions[0].z();
	back = mesh->positions[0].z();

	for (int i = 1; i < mesh->vertex_count; i++) {
		float x = mesh->positions[i].x();
		float y = mesh->positions[i].y();
		float z = mesh->positions[i].z();
		left = x < left ? x : left;
		right = x > right ? x : right;
		top = y < top ? y : top;
		bottom = y > bottom ? y : bottom;
		back = z < back ? z : back;
		front = z > front ? z : front;
	}

	BoundingBox box;
	box.p1 = Vector3 {left - bias, top - bias, back - bias};
	box.p2 = Vector3 {right + bias, bottom + bias, front + bias};
	return box;
}

void render_static_mesh_normals(GLuint program, Vector3 *camera_position, Vector3 *fog_color, StaticMesh *s_mesh, Matrix4 *world, Matrix4 *view, Matrix4 *projection) {
	GL_StaticMesh *mesh = (GL_StaticMesh *)s_mesh->plat_data;
	glUseProgram(program);
	glBindVertexArray(mesh->vao);

	glUniform3f(glGetUniformLocation(program, "camera_loc"), (*camera_position)[1], (*camera_position)[2], (*camera_position)[3]);
	glUniform3f(glGetUniformLocation(program, "fog_color"), (*fog_color)[1], (*fog_color)[2], (*fog_color)[3]);

	glUniformMatrix4fv(glGetUniformLocation(program, "world"), 1, GL_FALSE, (GLfloat *)world);
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, (GLfloat *)projection);
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, (GLfloat *)view);

	glDrawArrays(GL_POINTS, 0, mesh->vertex_count);
}

Matrix4 transform_to_matrix(Transform *transform) {
	Matrix4 mat = identity3D();
	mat = scale3D(transform->scale) * mat;
	mat = to_matrix(transform->rotation) * mat;
	mat = translate3D(transform->position) * mat;
	return mat;
}

#include <vector>

StaticMesh *load_obj(void **mem, const char *filepath) {
	FILE *file = fopen(filepath, "r");
	if (file == 0) {
		return 0;
	}

	StaticMesh *result = advance<StaticMesh>(mem, 1);

	{
		std::vector<Vector3> positions;
		std::vector<GLuint> indices;

		char buf[200];
		while (fgets(buf, sizeof(buf), file)) {
			Vector3 pos;
			unsigned int i1, i2, i3;

			if (sscanf(buf, "v %f %f %f", &pos.v[0], &pos.v[1], &pos.v[2])) {
				positions.push_back(pos);
			}
			else if (sscanf(buf, "f %d %d %d", &i1, &i2, &i3) == 3) {
				indices.push_back(i2 - 1);
				indices.push_back(i1 - 1);
				indices.push_back(i3 - 1);
			}
			//printf("%s", buf);
		}

		result = allocate_static_mesh(mem, positions.size(), indices.size());
		memcpy(result->positions, positions.data(), sizeof(Vector3) * result->vertex_count);
		memcpy(result->indices, indices.data(), sizeof(unsigned int) * result->index_count);

		calc_vertex_normals(result);
		init_platform_data(mem, result);
	}

	//init_platform_data();

	fclose(file);
	return result;
}

void calc_attentuation(float *out_a, float *out_b, float *out_c, float *out_far_plane) {
	// TODO(adam): Do culling for all the meshes, to see how far 
	// we REALLY need to push the back plane
	float a = 0.005f, b = 0.005f, c = 0.1f;
	float epsilon = 0.6f;
	float far_plane = (-b + sqrt(powf(b, 2.0f) - (4 * a * c - 4 * a / epsilon))) / (2 * a);

	*out_a = a;
	*out_b = b;
	*out_c = c;
	*out_far_plane = far_plane;
}

inline Matrix4 transform_to_matrix(Transform transform) {
	Matrix4 mat = identity3D();
	mat = scale3D(transform.scale) * mat;
	mat = to_matrix(transform.rotation) * mat;
	mat = translate3D(transform.position) * mat;
	return mat;
}

void render(GLuint material_program, StaticMesh *mesh, Transform *transform, Matrix4 *view, Matrix4 *projection) {

	GLuint program = material_program;
	glUseProgram(material_program);
	GL_StaticMesh *data = (GL_StaticMesh *)mesh->plat_data;

	glBindVertexArray(data->vao);

	Matrix4 world = transform_to_matrix(transform);

	glUniformMatrix4fv(glGetUniformLocation(program, "world"), 1, GL_FALSE, (GLfloat *)&world);
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, (GLfloat *)projection);
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, (GLfloat *)view);

	GLuint attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	glDrawBuffers(3, attachments);
	glDrawElements(GL_TRIANGLES, mesh->index_count, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
};


int main(int argc, char *argv[]) {

	if (!glfwInit()) {
		cout << "Failed to init glfw. Abort." << endl;
		return 1;
	}

	//glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 

	GLFWwindow *window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Depth", 0, 0);
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);


#ifndef __APPLE__
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		cout << "Failed to init glew. Abort." << endl;
		return 1;
	}
#endif

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetKeyCallback(window, on_keydown);
	//glfwSetCursorPosCallback(window, on_cursor_pos);

	glfwGetCursorPos(window, &inputs.mouse.last_x, &inputs.mouse.last_y);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glEnable(GL_BLEND);

	void *mem = malloc(1024 * 1024 * 30);
	StaticMesh *quad = create_quad((void **)&mem);
	StaticMesh *fullscreen_quad = create_quad((void **)&mem, 2, 2);


	//for (int i = 0; i < fullscreen_quad->vertex_count; i++) {
		//cout << fullscreen_quad->positions[i] << endl;
	//}
	StaticMesh *db_shadowmap_quad = create_quad((void **)&mem, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);

   	ImageData sand_img_data;
	load_image("resources/sand.png", &sand_img_data);

	GLuint skybox_texture;
	{
		ImageData posx, negx, posy, negy, posz, negz;
		load_image("resources/xpos.png", &posx);
		load_image("resources/xneg.png", &negx);
		load_image("resources/ypos.png", &posy);
		load_image("resources/yneg.png", &negy);
		load_image("resources/zpos.png", &posz);
		load_image("resources/zneg.png", &negz);
		//cout << posx.width << ", " << posx.height << endl;
		glGenTextures(1, &skybox_texture);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_texture);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA, posx.width, posx.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, posx.pixels);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA, negx.width, negx.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, negx.pixels);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA, posy.width, posy.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, posy.pixels);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA, negy.width, negy.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, negy.pixels);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA, posz.width, posz.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, posz.pixels);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA, negz.width, negz.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, negz.pixels);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	}
	StaticMesh *skybox_cube = create_cube(&mem, 2);

	/*
	StaticMesh *sand = allocate_static_mesh((void **)&mem, sand_img_data.width * sand_img_data.height, (sand_img_data.width - 1) * (sand_img_data.height - 1) * 6);

	{
		ImageData img_data = sand_img_data;
		Vector3 *cur_vector = sand->positions;
		uint32 *cur_index = sand->indices;
		float map_height = 5.0f;
		
		for (int x = 0; x < img_data.width; x++) {
			for (int y = 0; y < img_data.height; y++) {
				uint8 *pixel = img_data.pixels + ((y * img_data.width) + x) * 4;
				uint8 r = *(pixel + 0);
				*(cur_vector++) = Vector3 { (float)x, (float)y, ((float)r) / 255.0f * map_height };
			}
		}

		for (int x = 0; x < img_data.width - 1; x++) {
			for (int y = 0; y < img_data.height - 1; y++) {
				*(cur_index++) = ((y) * img_data.width) + x;
				*(cur_index++) = ((y) * img_data.width) + x + 1;
				*(cur_index++) = ((y + 1) * img_data.width) + x + 1;
				*(cur_index++) = ((y) * img_data.width) + x;
				*(cur_index++) = ((y + 1) * img_data.width) + x + 1;
				*(cur_index++) = ((y + 1) * img_data.width) + x;
			}
		}

		calc_vertex_normals(sand);

		init_platform_data((void **)&mem, sand);
	}*/

	StaticMesh *sphere = create_sphere(&mem, 60, 30);
	StaticMesh *cube = create_cube((void **)&mem);
	StaticMesh *box = create_box((void **)&mem, 5.0f, 2.0f, 3.0f);
	StaticMesh *box2 = create_box((void **)&mem, 30.0f, 1.0f, 30.0f);
/*
	struct Material {
		Vector3 color;
		Vector3 specular_color;
		float shininess;
		float specular_strength;
		Texture normal_map;
		Texture color;
		Texture specular_map;
	};

*/
	struct Actor {
		StaticMesh *mesh;
		//Material *Material;
		Transform transform;
		BoundingBox bb;
		bool casts_shadow;
		Actor(StaticMesh *in_mesh) {
			mesh = in_mesh;
			casts_shadow = true;
			bb = get_bounding_box(in_mesh);
		}
	};

	Actor *cube_actor = new Actor(cube);
	cube_actor->transform.rotation = create_quat(create_vec3(1.0f, -1.0f, 0.0f), -180.0f / 2.0f);
	cube_actor->transform.position = Vector3 {4.0f, 0.0f, -9.0f };

	Actor *box_actor = new Actor(box);
	box_actor->transform.position = Vector3 { 4.0f, -8.5f, -6.0f };
	box_actor->transform.rotation = y_quat(90.0f);

	//Actor *sand_actor = &actors[2];
	Actor *sphere2_actor = new Actor(sphere);
	sphere2_actor->transform.position = Vector3 { 5.0f, -2.0f, -5.0f };
	sphere2_actor->transform.rotation = y_quat(180.0f) * x_quat(90.0);
	sphere2_actor->transform.scale = Vector3 { 0.5f, 0.5f, 0.5f };

	Actor *sphere_actor = new Actor(sphere);
	sphere_actor->transform.position = Vector3 { 3.0f, -2.0f, -10.0f };

	Actor *box2_actor = new Actor(box2);
	box2_actor->transform.position = Vector3 { 4.0f, -10.0f, -6.0f };

	StaticMesh *dragon_mesh = load_obj(&mem, "resources/dragon.obj");
	Actor *dragon_actor = new Actor(dragon_mesh);
	dragon_actor->transform.scale = { 0.5f, 0.5f, 0.5f };
	dragon_actor->transform.position = { 0, -5, -5 };

	StaticMesh *buddha_mesh = load_obj(&mem, "resources/buddha.obj");
	Actor *buddha_actor = new Actor(buddha_mesh);
	buddha_actor->transform.scale = { 0.5f, 0.5f, 0.5f };
	buddha_actor->transform.position = { 7, -7, -5 };

	Actor *actors[] = {
		cube_actor,
		box_actor,
		sphere2_actor,
		sphere_actor,
		box2_actor,
		dragon_actor,
		buddha_actor,
	};
	int actor_count = array_size(actors);

	GLuint material_program;
	{
		GLuint vshader;
		if (!load_shader("resources/neutral/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}
		GLuint fshader;
		if (!load_shader("resources/neutral/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}
		GLuint shaders[] = {
			vshader,
			fshader,
		};

		if (!create_program(shaders, array_size(shaders), &material_program)) {
			return 1;
		}
	}

	GLuint plain_program;
	{
		GLuint vshader;
		if (!load_shader("resources/plain_diffuse/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}
		GLuint fshader;
		if (!load_shader("resources/plain_diffuse/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}
		GLuint shaders[] = {
			vshader,
			fshader,
		};

		if (!create_program(shaders, array_size(shaders), &plain_program)) {
			return 1;
		}
	}

	GLuint direct_lighting_program;
	{
		GLuint vshader;
		if (!load_shader("resources/direct_light/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}
		GLuint fshader;
		if (!load_shader("resources/direct_light/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}
		GLuint shaders[] = {
			vshader,
			fshader,
		};
		if (!create_program(shaders, array_size(shaders), &direct_lighting_program)) {
			return 1;
		}
	}

	GLuint skybox_program;
	{
		GLuint vshader;
		if (!load_shader("resources/skybox/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}
		GLuint fshader;
		if (!load_shader("resources/skybox/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}
		GLuint shaders[] = {
			vshader,
			fshader,
		};
		if (!create_program(shaders, array_size(shaders), &skybox_program)) {
			return 1;
		}
	}

	auto render_skybox = [skybox_program](StaticMesh *mesh, GLuint texture, Vector3 look_dir, Matrix4 look_matrix) {
		GLuint program = skybox_program;
		GL_StaticMesh *data = (GL_StaticMesh *)mesh->plat_data;

		glUseProgram(program);
		glBindVertexArray(data->vao);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
		glUniform1i(glGetUniformLocation(program, "tex"), 0);

		glUniform3fv(glGetUniformLocation(program, "look_dir"), 1, (GLfloat *)&look_dir);

		glUniformMatrix4fv(glGetUniformLocation(program, "look_matrix"), 1, GL_FALSE, (GLfloat *)&look_matrix);

		glDrawElements(GL_TRIANGLES, mesh->index_count, GL_UNSIGNED_INT, 0);
	};

	GLuint lighting_program;
	{
		GLuint vshader;
		if (!load_shader("resources/lighting/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}
		GLuint fshader;
		if (!load_shader("resources/lighting/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}
		/*
		GLuint fshader2;
		if (!load_shader("resources/lighting/calc_light.glsl", GL_FRAGMENT_SHADER, &fshader2)) {
			return 1;
		}
		GLuint fshader3;
		if (!load_shader("resources/lighting/point_light.glsl", GL_FRAGMENT_SHADER, &fshader3)) {
			return 1;
		}*/
		GLuint shaders[] = {
			vshader,
			fshader,
			//fshader2,
			//fshader3
		};
		if (!create_program(shaders, array_size(shaders), &lighting_program)) {
			return 1;
		}
	}

	GLuint texture;
	{
		ImageData img_data = sand_img_data;
		glGenTextures(1, &texture);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img_data.width, img_data.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img_data.pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	GLuint screen_quad_program;
	{
		GLuint vshader;
		if (!load_shader("resources/screen_quad/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}

		GLuint fshader;
		if (!load_shader("resources/screen_quad/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}

		GLuint shaders[2];
		shaders[0] = vshader;
		shaders[1] = fshader;
		//shaders[2] = gshader;

		if (!create_program(shaders, 2, &screen_quad_program)) {
			return 1;
		}
	}

	GLuint db_shadowmap_program;
	{
		GLuint vshader;
		if (!load_shader("resources/opaque_shadow_map/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}

		GLuint fshader;
		if (!load_shader("resources/opaque_shadow_map/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}

		GLuint *shaders = (GLuint *)malloc(sizeof(GLuint) * 2);
		shaders[0] = vshader;
		shaders[1] = fshader;
		//shaders[2] = gshader;

		if (!create_program(shaders, 2, &db_shadowmap_program)) {
			return 1;
		}

		free(shaders);
	}
	GLuint gbuffer_fbo;
	GLuint g_diffuse, g_normal, g_pos;
	GLuint g_render_buffer;
	{ // g-buffer
		int width = SCREEN_WIDTH;
		int height = SCREEN_HEIGHT;

		auto init_texture = [](GLuint *texture, int width, int height, GLenum i_format, GLenum format, GLenum type) {
			glGenTextures(1, texture);
			glBindTexture(GL_TEXTURE_2D, *texture);
			glTexImage2D(GL_TEXTURE_2D, 0, i_format, width, height, 0, format, type, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);  
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glBindTexture(GL_TEXTURE_2D, 0);
		};

		init_texture(&g_diffuse, width, height, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE);
		init_texture(&g_normal, width, height, GL_RGB32F, GL_RGB, GL_FLOAT);
		init_texture(&g_pos, width, height, GL_RGB32F, GL_RGB, GL_FLOAT);

		glGenRenderbuffers(1, &g_render_buffer);
		glBindRenderbuffer(GL_RENDERBUFFER, g_render_buffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);  
		glBindRenderbuffer(GL_RENDERBUFFER, 0);

		glGenFramebuffers(1, &gbuffer_fbo);
		glBindFramebuffer(GL_FRAMEBUFFER, gbuffer_fbo);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, g_diffuse, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, g_normal, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, g_pos, 0);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, g_render_buffer);
		GLuint attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
		glDrawBuffers(3, attachments);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	StaticMesh *gbuffer_db_quad = create_quad((void **)&mem, SCREEN_WIDTH, SCREEN_HEIGHT);

	GLuint program;
	{
		GLuint vshader;
		if (!load_shader("resources/default/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}

		GLuint fshader;
		if (!load_shader("resources/default/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}

		GLuint *shaders = (GLuint *)malloc(sizeof(GLuint) * 2);
		shaders[0] = vshader;
		shaders[1] = fshader;
		//shaders[2] = gshader;

		if (!create_program(shaders, 2, &program)) {
			return 1;
		}

		free(shaders);
	}

	GLuint db_program;
	{
		GLuint vshader;
		if (!load_shader("resources/debug_normals/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}

		GLuint fshader;
		if (!load_shader("resources/debug_normals/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}

		GLuint gshader;
		if (!load_shader("resources/debug_normals/geometry.glsl", GL_GEOMETRY_SHADER, &gshader)) {
			return 1;
		}

		GLuint *shaders = (GLuint *)malloc(sizeof(GLuint) * 3);
		shaders[0] = vshader;
		shaders[1] = fshader;
		shaders[2] = gshader;

		if (!create_program(shaders, 3, &db_program)) {
			return 1;
		}
	}

	GLuint omni_shadowmap_program;
	{
		GLuint vshader;
		if (!load_shader("resources/omni_shadow/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}

		GLuint fshader;
		if (!load_shader("resources/omni_shadow/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}

		GLuint gshader;
		if (!load_shader("resources/omni_shadow/geometry.glsl", GL_GEOMETRY_SHADER, &gshader)) {
			return 1;
		}

		GLuint shaders[] = {
			vshader,
			fshader,
			gshader
		};

		if (!create_program(shaders, 3, &omni_shadowmap_program)) {
			return 1;
		}
	}

	auto render_omni_shadow = [omni_shadowmap_program](StaticMesh *mesh, Transform *transform, Vector3 *light_pos, Shadowmap *map, float radius) {
		GLuint program = omni_shadowmap_program;
		GL_StaticMesh *data = (GL_StaticMesh *)mesh->plat_data;

		//float a, b, c, far_plane;
		//calc_attentuation(&a, &b, &c, &far_plane);
		float far_plane = radius;

		Matrix4 world = transform_to_matrix(*transform);
		float fov_x = 90.0f * M_PI / 180.0f;
		Matrix4 proj = perspective3D(fov_x, fov_x * (float)map->height / (float)map->width, 0.1f, far_plane);
		Matrix4 surface_vp[] = {
			proj * look_at(*light_pos, *light_pos + Vector3 { 1.0f,  0.0f,  0.0f}, Vector3 {0.0f, -1.0f, 0.0f}),
			proj * look_at(*light_pos, *light_pos + Vector3 {-1.0f,  0.0f,  0.0f}, Vector3 {0.0f, -1.0f, 0.0f}),
			proj * look_at(*light_pos, *light_pos + Vector3 { 0.0f,  1.0f,  0.0f}, Vector3 {0.0f,  0.0f, 1.0f}),
			proj * look_at(*light_pos, *light_pos + Vector3 { 0.0f, -1.0f,  0.0f}, Vector3 {0.0f,  0.0f, -1.0f}),
			proj * look_at(*light_pos, *light_pos + Vector3 { 0.0f,  0.0f,  1.0f}, Vector3 {0.0f, -1.0f, 0.0f}),
			proj * look_at(*light_pos, *light_pos + Vector3 { 0.0f,  0.0f, -1.0f}, Vector3 {0.0f, -1.0f, 0.0f}),
		};

		glBindVertexArray(data->vao);
		glUseProgram(program);

		glUniform1f(glGetUniformLocation(program, "far_plane"), far_plane);
		glUniform3fv(glGetUniformLocation(program, "light_pos"), 1, (GLfloat *)light_pos);
		glUniformMatrix4fv(glGetUniformLocation(program, "world"), 1, GL_FALSE, (GLfloat *)&world);
		glUniformMatrix4fv(glGetUniformLocation(program, "surface_matrices"), 6, GL_FALSE, (GLfloat *)surface_vp);

		glDrawElements(GL_TRIANGLES, mesh->index_count, GL_UNSIGNED_INT, 0);
		
	};

	GLuint direct_shadow_program;
	{
		GLuint vshader;
		if (!load_shader("resources/opaque_shadow_map/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}

		GLuint fshader;
		if (!load_shader("resources/opaque_shadow_map/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}

		GLuint shaders[] = {
			vshader,
			fshader,
		};

		if (!create_program(shaders, array_size(shaders), &direct_shadow_program)) {
			return 1;
		}
	}

	auto render_direct_shadow = [direct_shadow_program](StaticMesh *mesh, Transform *transform, Vector3 *direction) {
		GLuint program = direct_shadow_program;
		GL_StaticMesh *data = (GL_StaticMesh *)mesh->plat_data;
		glUseProgram(program);
		glBindVertexArray(data->vao);

		Matrix4 view, projection;
		calc_light_matrices(direction, &view, &projection);
		Matrix4 light_pv = projection * view;

		Matrix4 world = transform_to_matrix(transform);

		glUniformMatrix4fv(glGetUniformLocation(program, "world"), 1, GL_FALSE, (GLfloat *)&world);
		//glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, (GLfloat *)&view);
		//glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, (GLfloat *)&projection);
		glUniformMatrix4fv(glGetUniformLocation(program, "light_pv"), 1, GL_FALSE, (GLfloat *)&light_pv);

		glDrawElements(GL_TRIANGLES, mesh->index_count, GL_UNSIGNED_INT, 0);
	};

	float angle = 0.0f;
	float last = glfwGetTime();

	Vector3 fog_color = { 0.3f, 0.3f, 1.0f };
	//Vector3 light_pos = { 30.0f, 40.0f, 30.0f };

	Shadowmap *omni_shadowmap = allocate_omni_shadowmap(&mem, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);
	Shadowmap *direct_shadowmap = allocate_direct_shadowmap(&mem, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);

	PointLights *plights = allocate_point_lights((void **)&mem, 9);
	{
		plights->colors[0] = {0.5f, 0.5f, 0.5f};
		plights->positions[0] = {5.0f, 5.0f, -10.0f};
		plights->active[0] = true;
		plights->shadowing[0] = true;
		plights->colors[1] = {0.8f, 0.6f, 0.5f};
		plights->active[1] = true;
		plights->shadowing[1] = true;
		plights->radii[1] = 20.0f;
		plights->intensities[1] = 0.6f;
		plights->positions[1] = { 0.0f, -5.f, 0.0f };
		//plights->active[2] = true;
		//plights->colors[2] = {0.2, 0.2, 0.7};
		//
		//for (int i = 0; i < plights->count; i++) {
			//plights->active[i] = true;
		//}
	}

	struct PrimitiveContex {
		Vector3 *positions;
		int size;
		int cur_pos;

		GLuint program;
		GLuint vao;
		GLuint vbo;
		GLint color_loc;
		GLint view_loc;
		GLint projection_loc;
	} primitive;

	{
		GLuint vshader;
		if (!load_shader("resources/primitive/vertex.glsl", GL_VERTEX_SHADER, &vshader)) {
			return 1;
		}
		GLuint fshader;
		if (!load_shader("resources/primitive/fragment.glsl", GL_FRAGMENT_SHADER, &fshader)) {
			return 1;
		}
		GLuint shaders[] = {
			vshader,
			fshader,
		};

		if (!create_program(shaders, array_size(shaders), &primitive.program)) {
			return 1;
		}
		//glDeleteShader(vshader);
		//glDeleteShader(fshader);

		primitive.color_loc = glGetUniformLocation(primitive.program, "color");
		primitive.view_loc = glGetUniformLocation(primitive.program, "view");
		primitive.projection_loc = glGetUniformLocation(primitive.program, "projection");
		//cout << primitive.color_loc << endl;
		//cout << primitive.view_loc << endl;
		//cout << primitive.projection_loc << endl;
	}

	primitive.cur_pos = 0;
	primitive.size = 100;
	primitive.positions = advance<Vector3>(&mem, primitive.size);
	for (int i = 0; i < 16; i++) {
		primitive.positions[i] = {i * 1.0f, i * 1.0f, i * 1.0f};
	}

	//primitive.positions = 0;
	glGenVertexArrays(1, &primitive.vao);
	glBindVertexArray(primitive.vao);
	glGenBuffers(1, &primitive.vbo);
	glBindBuffer(GL_ARRAY_BUFFER, primitive.vbo);
	glBufferData(GL_ARRAY_BUFFER,
			primitive.size * sizeof(Vector3),
			0,
			GL_DYNAMIC_DRAW
		);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);

	auto purge = [](PrimitiveContex *contex) {
		contex->cur_pos = 0;
	};

	auto push_point = [](PrimitiveContex *context, Vector3 point) {
		//Vector3 point = { x, y, z };
		context->positions[context->cur_pos++] = point;
	};

	auto draw = [](PrimitiveContex *context, Matrix4 *view, Matrix4 *projection){
		glUseProgram(context->program);
		glBindBuffer(GL_ARRAY_BUFFER, context->vbo);
		glBufferSubData(GL_ARRAY_BUFFER,
				0,
				context->cur_pos * sizeof(Vector3),
				context->positions
			);
		glBindVertexArray(context->vao);
		Vector3 color = { 1.0f, 0.0f, 0.0f };
		glUniform3fv(context->color_loc, 1, (GLfloat *)&color);
		glUniformMatrix4fv(context->view_loc, 1, GL_FALSE, (GLfloat *)view);
		glUniformMatrix4fv(context->projection_loc, 1, GL_FALSE, (GLfloat *)projection);
		glDrawArrays(GL_LINES, 0, 100);
		glBindVertexArray(0);
	};

/*
	SkyboxShader shader;
	init(&shader);
	shader.use();
	draw(fullscreen_quad);

	PlainMaterial material;
	init(&material);
	NanosuitMaterial nano_material;
	init(&nano_material);

	material.use();
	glBindFramebuffer(GL_FRAMEBUFFER, gbuffer_fbo);
	draw(mesh);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
*/
	DirectLights *dlights = allocate_direct_lights((void **)&mem, 1);
	{
		dlights->colors[0] = {0.0f, 0.5f, 0.0f};
		//dlights->colors[0] = normalize(Vector3 {1.0f, 1.0f, 1.0f});
		//dlights->active[0] = true;
		dlights->shadowing[0] = true;
	}

	while (!glfwWindowShouldClose(window))
	{
		float now = glfwGetTime();
		float dt = now - last;
		last = now;

		{ // inputs
			double x, y;
			glfwGetCursorPos(window, &x, &y);
			inputs.mouse.dx = (float)(x - inputs.mouse.last_x);
			inputs.mouse.dy = (float)(y - inputs.mouse.last_y);
			inputs.mouse.last_x = x;
			inputs.mouse.last_y = y;
		}

		{ // control
			camera.rotation.x += inputs.mouse.dy / 10.0f;
			if (camera.rotation.x > 90.0f) {
				camera.rotation.x = 90.0f;
			}
			if (camera.rotation.x < -90.0f) {
				camera.rotation.x = -90.0f;
			}
			camera.rotation.y += inputs.mouse.dx / 10.0f;

			Quat orientation = y_quat(camera.rotation.y);

			float speed = 5.0f;
			auto move_dir = Vector3::empty();
			if (inputs.left && !inputs.right) {
				move_dir += (orientation * create_vec4(-1.0f, 0.0f, 0.0f, 0.0f)).xyz();
			}
			else if (inputs.right && !inputs.left) {
				move_dir += (orientation * create_vec4(1.0f, 0.0f, 0.0f, 0.0f)).xyz();
			}
			if (inputs.forward && !inputs.backward) {
				move_dir += (orientation * create_vec4(0.0f, 0.0f, -1.0f, 0.0f)).xyz();
			}
			else if (inputs.backward && !inputs.forward) {
				move_dir += (orientation * create_vec4(0.0f, 0.0f, 1.0f, 0.0f)).xyz();
			}
			if (inputs.up && !inputs.down) {
				move_dir += Vector3 { 0.0f, 1.0f, 0.0f };
			}
			else if (inputs.down && !inputs.up) {
				move_dir += Vector3 { 0.0f, -1.0f, 0.0f };
			}

			move_dir = normalize(move_dir);
			camera.position += move_dir * speed * dt;
		}

		cube_actor->transform.rotation = create_quat(create_vec3(0.0f, 0.0f, 1.0f), 120.0f * dt) * cube_actor->transform.rotation;
		dragon_actor->transform.rotation = create_quat(create_vec3(0.0f, 1.0f, 0.0f), 120.0f * dt) * dragon_actor->transform.rotation;
		buddha_actor->transform.rotation = create_quat(create_vec3(0.0f, 1.0f, 0.0f), -120.0f * dt) * buddha_actor->transform.rotation;

		//{ // shadow-map
			//GL_DirectLights *data = (GL_DirectLights *)dlights->plat_data;
			//glBindFramebuffer(GL_FRAMEBUFFER, data->shadow_fbos[0]);
			//glViewport(0, 0, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);

			//Matrix4 view, projection;
			//calc_light_matrices(&dlights->directions[0], &view, &projection);

			//glClear(GL_DEPTH_BUFFER_BIT);

			//for (int i = 0; i < actor_count; i++) {
				//Matrix4 world = transform_to_matrix(actors[i].transform);
				//render_shadow_map(db_shadowmap_program, actors[i].mesh, &world, &view, &projection);
			//}

			//glBindFramebuffer(GL_FRAMEBUFFER, 0);
		/*}*/

		glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		Matrix4 projection = perspective3D(84.0f * M_PI / 180.0f, 60.0f * M_PI / 180.0f, 1.0f, 100.0f);
		//Matrix4 projection = ortho3D(30.0f, 15.0f, 0.0f, 1000.0f);

		Matrix4 view = identity3D();
		{
			Quat orientation = y_quat(camera.rotation.y) * x_quat(camera.rotation.x);
			Matrix4 inverse_view = identity3D();
			inverse_view = to_matrix(orientation) * inverse_view;
			inverse_view = translate3D(camera.position) * inverse_view;
			view = invert(inverse_view);
		}
		{
			//view = look_at(camera.position, sand_actor->transform.position, Vector3 {-1.0f, 1.0f, 0.0f});
		}

		{ // #g-buffer
			glEnable(GL_DEPTH_TEST);
			glBindFramebuffer(GL_FRAMEBUFFER, gbuffer_fbo);
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			for (int i = 0; i < actor_count; i++) {
				render(material_program, actors[i]->mesh, &actors[i]->transform, &view, &projection);
			}
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}

		//glClearColor(fog_color[1], fog_color[2], fog_color[3], 1.0f );
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f );
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		//for (int i = 0; false && i < actor_count; i++) { // #forward-rendering
			//Matrix4 world = transform_to_matrix(actors[i].transform);
			//render_untextured_static_mesh(program, plights, dlights, &camera.position, &fog_color, actors[i].mesh, &world, &view, &projection);
		/*}*/

		{ // Deferred Lighting pass
			glDisable(GL_DEPTH_TEST);

			StaticMesh *mesh = fullscreen_quad;
			GL_StaticMesh *data = (GL_StaticMesh *)mesh->plat_data;

			for (int i = 0; i < dlights->count; i++) {
				if (!dlights->active[i]) { continue; }
				GL_Shadowmap *direct_shadowmap_data = (GL_Shadowmap *)direct_shadowmap->plat_data;
				glBindFramebuffer(GL_FRAMEBUFFER, direct_shadowmap_data->fbo);
				glViewport(0, 0, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);
				glEnable(GL_DEPTH_TEST);
					//glDepthMask(GL_TRUE);
				glClear(GL_DEPTH_BUFFER_BIT);

				if (dlights->shadowing[i]) {

					for (int j = 0; j < actor_count; j++) {
						if (!actors[j]->casts_shadow) { continue; }
						render_direct_shadow(actors[j]->mesh, &actors[j]->transform, &dlights->directions[i]);
					}

				}
				glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
				glDisable(GL_DEPTH_TEST);
				glBindFramebuffer(GL_FRAMEBUFFER, 0);

				GLuint program = direct_lighting_program;
				glUseProgram(program);
				glBindVertexArray(data->vao);
				//glBindBuffer(GL_ARRAY_BUFFER, data->vbo);
				//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data->ebo);

				glEnable(GL_BLEND);
				glBlendFunc(GL_ONE, GL_ONE);
				//glBlendEquation(GL_MAX);

				
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, g_diffuse);
				glUniform1i(glGetUniformLocation(program, "g_diffuse"), 0);
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, g_normal);
				glUniform1i(glGetUniformLocation(program, "g_normal"), 1);
				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, g_pos);
				glUniform1i(glGetUniformLocation(program, "g_pos"), 2);

				Matrix4 view, projection;
				calc_light_matrices(&dlights->directions[i], &view, &projection);
				Matrix4 light_pv = projection * view;

				glUniformMatrix4fv(glGetUniformLocation(program, "light_pv"), 1, GL_FALSE, (GLfloat *)&light_pv);
				glUniform3fv(glGetUniformLocation(program, "camera_pos"), 1, (GLfloat *)&camera.position);
				//cout << glGetUniformLocation(program, "far_plane") << endl;
				//glUniform1f(glGetUniformLocation(program, "far_plane"), LIGHT_FAR_PLANE);
				//cout << glGetUniformLocation(program, "far_plane") << endl;
				//if (dlights->shadowing[i]) {
					glActiveTexture(GL_TEXTURE3);
					glBindTexture(GL_TEXTURE_2D, direct_shadowmap_data->texture);
					//cout << glGetUniformLocation(program, "shadow_map") << endl;
					glUniform1i(glGetUniformLocation(program, "shadow_map"), 3);
					//glActiveTexture(GL_TEXTURE4);
					//glBindTexture(GL_TEXTURE_2D, direct_shadowmap_data->texture);
					//cout << glGetUniformLocation(program, "shadow_map_debug") << endl;
					//glUniform1i(glGetUniformLocation(program, "shadow_map_debug"), 4);
					dump_gl_error();
				//}

				glUniform3fv(glGetUniformLocation(program, "light_dir"), 1, (GLfloat *)&dlights->directions[i]);
				//glUniform3f(glGetUniformLocation(program, "light_pos"), plights->positions[i][1], plights->positions[i][2], plights->positions[i][3]);
				glUniform3fv(glGetUniformLocation(program, "light_color"), 1, (GLfloat *)&dlights->colors[i]);
				glDrawElements(GL_TRIANGLES, mesh->index_count, GL_UNSIGNED_INT, 0);
				glDisable(GL_BLEND);

			}

			for (int i = 0; i < plights->count; i++) {
				if (!plights->active[i]) { continue; }
				//if (plights->shadowing[i]) { continue; }
				GL_Shadowmap *omni_shadowmap_data = (GL_Shadowmap *)omni_shadowmap->plat_data;
				glBindFramebuffer(GL_FRAMEBUFFER, omni_shadowmap_data->fbo);
				glViewport(0, 0, SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);
				glEnable(GL_DEPTH_TEST);
				glClear(GL_DEPTH_BUFFER_BIT);
				if (plights->shadowing[i]) {
					//glDepthMask(GL_TRUE);
					//glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
					for (int j = 0; j < actor_count; j++) {
						if (!actors[j]->casts_shadow) { continue; }
						render_omni_shadow(actors[j]->mesh, &actors[j]->transform, &plights->positions[i], omni_shadowmap, plights->radii[i]);
					}
				}
				glBindFramebuffer(GL_FRAMEBUFFER, 0);
				glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
				glDisable(GL_DEPTH_TEST);

				GLuint program = lighting_program;
				glUseProgram(program);
				glBindVertexArray(data->vao);
				//glBindBuffer(GL_ARRAY_BUFFER, data->vbo);
				//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data->ebo);

				glEnable(GL_BLEND);
				glBlendFunc(GL_ONE, GL_ONE);
				//glBlendEquation(GL_MAX);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, g_diffuse);
				glUniform1i(glGetUniformLocation(program, "g_diffuse"), 0);
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, g_normal);
				glUniform1i(glGetUniformLocation(program, "g_normal"), 1);
				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, g_pos);
				glUniform1i(glGetUniformLocation(program, "g_pos"), 2);

				//float a, b, c, far_plane;
				//calc_attentuation(&a, &b, &c, &far_plane);
				//glUniform1f(glGetUniformLocation(program, "quadratic"), a);
				//glUniform1f(glGetUniformLocation(program, "linear"), b);
				//glUniform1f(glGetUniformLocation(program, "constant"), c);
				glUniform1f(glGetUniformLocation(program, "radius"), plights->radii[i]);
				glUniform1f(glGetUniformLocation(program, "intensity"), plights->intensities[i]);
				//cout << plights->radii[i] << endl;

				glUniform3f(glGetUniformLocation(program, "camera_pos"), camera.position[1], camera.position[2], camera.position[3]);

				//float far_plane = lights->radii[i];
				float far_plane = plights->radii[i];
				//cout << glGetUniformLocation(program, "far_plane") << endl;
				glUniform1f(glGetUniformLocation(program, "far_plane"), far_plane);
				//cout << glGetUniformLocation(program, "far_plane") << endl;
				//if (plights->shadowing[i]) {
					glActiveTexture(GL_TEXTURE3);
					glBindTexture(GL_TEXTURE_CUBE_MAP, omni_shadowmap_data->texture);
					glUniform1i(glGetUniformLocation(program, "shadow_map"), 3);
					//cout << glGetUniformLocation(program, "shadow_map") << endl;
					glActiveTexture(GL_TEXTURE4);
					glBindTexture(GL_TEXTURE_CUBE_MAP, omni_shadowmap_data->texture);
					glUniform1i(glGetUniformLocation(program, "shadow_map_debug"), 4);
					//cout << glGetUniformLocation(program, "shadow_map") << endl;
				//}

				glUniform3f(glGetUniformLocation(program, "light_pos"), plights->positions[i][1], plights->positions[i][2], plights->positions[i][3]);
				glUniform3f(glGetUniformLocation(program, "light_color"), plights->colors[i][1], plights->colors[i][2], plights->colors[i][3]);
				glDrawElements(GL_TRIANGLES, mesh->index_count, GL_UNSIGNED_INT, 0);
				glDisable(GL_BLEND);
			}

			glDisable(GL_BLEND);
			glEnable(GL_DEPTH_TEST);
		}

		if (inputs.draw_shadow_map) {
			glViewport(0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT);
			glDisable(GL_DEPTH_TEST);
			GL_Shadowmap *direct_shadowmap_data = (GL_Shadowmap *)direct_shadowmap->plat_data;
			render_screen_quad(screen_quad_program, db_shadowmap_quad, direct_shadowmap_data->texture, 0, 0);
			glEnable(GL_DEPTH_TEST);
		}

		if (inputs.gbuffer_draw > 0) {
			GLuint texture = g_diffuse;
			switch(inputs.gbuffer_draw) {
				case 1:
					texture = g_diffuse;
					break;
				case 2:
					texture = g_normal;
					break;
				case 3:
					texture = g_pos;
					break;
			}

			glDisable(GL_DEPTH_TEST);
			render_screen_quad(screen_quad_program, gbuffer_db_quad, texture, 0, 0);
			glEnable(GL_DEPTH_TEST);
		}

		{ // Skybox
			glBindFramebuffer(GL_READ_FRAMEBUFFER, gbuffer_fbo);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			glBlitFramebuffer(
				0, 0, SCREEN_WIDTH, SCREEN_HEIGHT,
				0, 0, SCREEN_WIDTH, SCREEN_HEIGHT,
				GL_DEPTH_BUFFER_BIT , GL_NEAREST
			);
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LEQUAL);
			glDisable(GL_CULL_FACE);
			glViewport(0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT);
			Quat orientation = y_quat(camera.rotation.y) * x_quat(camera.rotation.x);
			//Matrix4 look_matrix = ortho3D(20.0f, 20.0f, 1.0, 20.0) * invert(to_matrix(orientation));
			Matrix4 look_matrix = projection * invert(to_matrix(orientation));
			//Matrix4 look_matrix = view;

			render_skybox(skybox_cube, skybox_texture, (orientation * Vector4 {0.0f, 0.0f, -1.0f, 0.0f}).xyz(), look_matrix);
			//glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glDepthFunc(GL_LESS);
		}

		// Drawing positions of lights
		if (inputs.draw_lights) {

			glEnable(GL_DEPTH_TEST);
			glBindFramebuffer(GL_READ_FRAMEBUFFER, gbuffer_fbo);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			glBlitFramebuffer(
				0, 0, SCREEN_WIDTH, SCREEN_HEIGHT,
				0, 0, SCREEN_WIDTH, SCREEN_HEIGHT,
				GL_DEPTH_BUFFER_BIT , GL_NEAREST
			);
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			for (uint32 i = 0; i < plights->count; i++) {
				if (!plights->active[i]) { continue; }
				GL_StaticMesh *data = (GL_StaticMesh *)sphere->plat_data;

				GLuint program = plain_program;
				glUseProgram(program);
				glBindVertexArray(data->vao);

				Matrix4 pvw = projection * view * translate3D(plights->positions[i]);
				glUniformMatrix4fv(glGetUniformLocation(program, "pvw"), 1, GL_FALSE, (GLfloat *)&pvw);

				Vector3 *color = &plights->colors[i];
				glUniform3f(glGetUniformLocation(program, "color"), color->x(), color->y(), color->z());

				glDrawElements(GL_TRIANGLES, sphere->index_count, GL_UNSIGNED_INT, 0);

				glBindVertexArray(0);
			}
		}

#if 0
		{ // Bounding boxes for meshes
			glBindFramebuffer(GL_READ_FRAMEBUFFER, gbuffer_fbo);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			glBlitFramebuffer(
				0, 0, SCREEN_WIDTH, SCREEN_HEIGHT,
				0, 0, SCREEN_WIDTH, SCREEN_HEIGHT,
				GL_DEPTH_BUFFER_BIT , GL_NEAREST
			);
			glDisable(GL_CULL_FACE);
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glViewport(0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT);
			glEnable(GL_DEPTH_TEST);


			for (int i = 0; i < actor_count; i++) {
				purge(&primitive);
				
				Actor *actor = actors[i];
				Matrix4 world = transform_to_matrix(&actor->transform);
				Vector3 p1 = actor->bb.p1;
				Vector3 p2 = actor->bb.p2;

				push_point(&primitive, world * Vector3 { p1.x(), p1.y(), p1.z() }); // top
				push_point(&primitive, world * Vector3 { p2.x(), p1.y(), p1.z() });
				push_point(&primitive, world * Vector3 { p2.x(), p1.y(), p1.z() });
				push_point(&primitive, world * Vector3 { p2.x(), p1.y(), p2.z() });
				push_point(&primitive, world * Vector3 { p2.x(), p1.y(), p2.z() });
				push_point(&primitive, world * Vector3 { p1.x(), p1.y(), p2.z() });
				push_point(&primitive, world * Vector3 { p1.x(), p1.y(), p2.z() });
				push_point(&primitive, world * Vector3 { p1.x(), p1.y(), p1.z() });
				push_point(&primitive, world * Vector3 { p1.x(), p2.y(), p1.z() }); // bottom
				push_point(&primitive, world * Vector3 { p2.x(), p2.y(), p1.z() });
				push_point(&primitive, world * Vector3 { p2.x(), p2.y(), p1.z() });
				push_point(&primitive, world * Vector3 { p2.x(), p2.y(), p2.z() });
				push_point(&primitive, world * Vector3 { p2.x(), p2.y(), p2.z() });
				push_point(&primitive, world * Vector3 { p1.x(), p2.y(), p2.z() });
				push_point(&primitive, world * Vector3 { p1.x(), p2.y(), p2.z() });
				push_point(&primitive, world * Vector3 { p1.x(), p2.y(), p1.z() });
				push_point(&primitive, world * Vector3 { p1.x(), p1.y(), p1.z() }); // pillar
				push_point(&primitive, world * Vector3 { p1.x(), p2.y(), p1.z() });
				push_point(&primitive, world * Vector3 { p1.x(), p1.y(), p2.z() }); // pillar
				push_point(&primitive, world * Vector3 { p1.x(), p2.y(), p2.z() });
				push_point(&primitive, world * Vector3 { p2.x(), p1.y(), p2.z() }); // pillar
				push_point(&primitive, world * Vector3 { p2.x(), p2.y(), p2.z() });
				push_point(&primitive, world * Vector3 { p2.x(), p1.y(), p1.z() }); // pillar
				push_point(&primitive, world * Vector3 { p2.x(), p2.y(), p1.z() });

				draw(&primitive, &view, &projection);
			}
			glEnable(GL_CULL_FACE);
		}

#endif
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();

	return 0;
}

