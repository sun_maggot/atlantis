#include "atlantis.h"
#include "transform3D.h"

StaticMesh *create_sphere(void **mem, int lat_count, int lon_count) {
	// TODO(adam): add the missing bottom most vertex
	float radius = 1.0f;
	StaticMesh *mesh = allocate_static_mesh(mem, (lat_count + 1) * (lon_count), (lat_count) * (lon_count) * 6);

	for (uint32 lat = 0; lat < lat_count + 1; lat++) {
		for (uint32 lon = 0; lon < lon_count; lon++) {
			float y = radius * cosf(M_PI * ((float)lat) / (float)lat_count);
			float x = radius * sinf(M_PI * ((float)lat) / (float)lat_count) * cosf(M_PI * 2 * ((float)lon) / (float)lon_count);
			float z = radius * sinf(M_PI * ((float)lat) / (float)lat_count) * sinf(M_PI * 2 * ((float)lon) / (float)lon_count);
			mesh->positions[(lat * lon_count) + lon] = { x, y, z };
			mesh->uv[(lat * lon_count) + lon] = { ((float)lon) / ((float)lon_count), ((float)lat) / ((float)lat_count) };
		}
	}

	uint32 index_count = 0;
	for (uint32 i = 0; i < (lat_count + 1) - 1; i++) {
		for (uint32 j = 0; j < lon_count; j++) {
			uint32 is[] = {
				i * lon_count + j, i * lon_count + (j + 1 < lon_count ? j + 1 : 0),
				(i + 1) * lon_count + j, (i + 1) * lon_count + (j + 1 < lon_count ? j + 1 : 0),
			};

			mesh->indices[index_count++] = is[1];
			mesh->indices[index_count++] = is[0];
			mesh->indices[index_count++] = is[2];
			mesh->indices[index_count++] = is[1];
			mesh->indices[index_count++] = is[2];
			mesh->indices[index_count++] = is[3];
		}
	}
	calc_vertex_normals(mesh);
	//cout << index_count << endl;
	for (int i = 0; i < mesh->vertex_count; i++) {
		//cout << mesh->normals[i].x() << ", " << mesh->normals[i].y() << ", " << mesh->normals[i].z() << endl;
	}

	init_platform_data(mem, mesh);
	return mesh;
}

StaticMesh *create_quad(void **mem, int width, int height) {
	StaticMesh *mesh = allocate_static_mesh(mem, 4, 6);

	float w = width * 0.5;
	float h = height * 0.5;

	Vector3 *positions = mesh->positions;
	positions[0] = { -w,  h, 0.0f };
	positions[1] = {  w,  h, 0.0f };
	positions[2] = {  w, -h, 0.0f };
	positions[3] = { -w, -h, 0.0f };

	Vector3 *normals = mesh->normals;
	normals[0] = { 0.0f, 0.0f, 1.0f };
	normals[1] = { 0.0f, 0.0f, 1.0f };
	normals[2] = { 0.0f, 0.0f, 1.0f };
	normals[3] = { 0.0f, 0.0f, 1.0f };

	Vector2 *uv = mesh->uv;
	uv[0] = { 0.0f, 1.0f };
	uv[1] = { 1.0f, 1.0f };
	uv[2] = { 1.0f, 0.0f };
	uv[3] = { 0.0f, 0.0f };

	uint32 *indices = mesh->indices;
	{
		uint32 indices_data[6] = {0, 1, 2, 0, 2, 3};
		for (int i = 0; i < 6; i++) {
			indices[i] = indices_data[i];
		}
	}
	init_platform_data(mem, mesh);
	return mesh;
}

StaticMesh *create_box(void **mem, float width, float height, float depth) {
	StaticMesh *mesh = allocate_static_mesh(mem, 4 * 6, 6 * 6);

	width *= 0.5f;
	height *= 0.5f;
	depth *= 0.5f;

	float size = 0.5f;
	Vector<6> vdata[24] = {
		{ -width,  height,  depth,  0.0f,  0.0f,  1.0f },
		{  width,  height,  depth,  0.0f,  0.0f,  1.0f },
		{  width, -height,  depth,  0.0f,  0.0f,  1.0f },
		{ -width, -height,  depth,  0.0f,  0.0f,  1.0f },

		{  width,  height, -depth,  0.0f,  0.0f, -1.0f },
		{ -width,  height, -depth,  0.0f,  0.0f, -1.0f },
		{ -width, -height, -depth,  0.0f,  0.0f, -1.0f },
		{  width, -height, -depth,  0.0f,  0.0f, -1.0f },

		{  width,  height,  depth,  1.0f,  0.0f,  0.0f },
		{  width,  height, -depth,  1.0f,  0.0f,  0.0f },
		{  width, -height, -depth,  1.0f,  0.0f,  0.0f },
		{  width, -height,  depth,  1.0f,  0.0f,  0.0f },

		{ -width,  height, -depth, -1.0f,  0.0f,  0.0f },
		{ -width,  height,  depth, -1.0f,  0.0f,  0.0f },
		{ -width, -height,  depth, -1.0f,  0.0f,  0.0f },
		{ -width, -height, -depth, -1.0f,  0.0f,  0.0f },

		{  width,  height,  depth,  0.0f,  1.0f,  0.0f },
		{ -width,  height,  depth,  0.0f,  1.0f,  0.0f },
		{ -width,  height, -depth,  0.0f,  1.0f,  0.0f },
		{  width,  height, -depth,  0.0f,  1.0f,  0.0f },

		{  width, -height, -depth,  0.0f, -1.0f,  0.0f },
		{ -width, -height, -depth,  0.0f, -1.0f,  0.0f },
		{ -width, -height,  depth,  0.0f, -1.0f,  0.0f },
		{  width, -height,  depth,  0.0f, -1.0f,  0.0f },
	};

	Vector3 *positions = mesh->positions;
	Vector3 *normals = mesh->normals;
	for (int i = 0; i < 6 * 4; i++) {
		positions[i] = { vdata[i][1], vdata[i][2], vdata[i][3] };
		normals[i] = { vdata[i][4], vdata[i][5], vdata[i][6] };
	}

	uint32 *indices = mesh->indices;
	for (uint32 i = 0; i < 6; i++) {
		uint32 index = i * 4;
		uint32 j = i * 6;
		indices[j+0] = index;
		indices[j+1] = index + 1;
		indices[j+2] = index + 2;
		indices[j+3] = index;
		indices[j+4] = index + 2;
		indices[j+5] = index + 3;
	}

	init_platform_data(mem, mesh);
	return mesh;
}

StaticMesh *create_cube(void **mem, float size) {
	return create_box(mem, size, size, size);
}
