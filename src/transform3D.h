#ifndef TRANSFORM_3D_H
#define TRANSFORM_3D_H

#include "transform.h"
#include "vector.h"
#include <cmath>

typedef Vector<2> Vector2;
typedef Vector<3> Vector3;
typedef Vector<4> Vector4;
typedef Matrix<4, 4> Matrix4;

inline Vector3 create_vec3(float x, float y, float z) {
	return { x, y, z };
}

inline Vector4 create_vec4(float x, float y, float z, float w) {
	return { x, y, z, w };
}

inline Vector4 create_vec4(const Vector3& vec3, float w) {
	return { vec3[1], vec3[2], vec3[3], w };
}

inline Vector3 multiply(const Matrix4 &mat, const Vector3 &vec) {
	Vector<4> calc_vec = { vec.at(1), vec.at(2), vec.at(3), 1.0f };
	Vector<4> result = mat * calc_vec;
	return { result.at(1), result.at(2), result.at(3) };
}

inline Vector3 operator*(const Matrix4 &mat, const Vector3 &vec) {
	return multiply(mat, vec);
}

inline Matrix4 identity3D() {
	return identity_matrix<4>();
}

inline Matrix4 translate3D(float x, float y, float z) {
	Matrix4 mat = identity3D();
	mat.at(1, 4) = x;
	mat.at(2, 4) = y;
	mat.at(3, 4) = z;
	return mat;
}

inline Matrix4 translate3D(const Vector3& vec) {
	return translate3D(vec[1], vec[2], vec[3]);
}

inline Matrix4 scale3D(float x, float y, float z) {
	Matrix4 mat = identity3D();
	mat.at(1, 1) = x;
	mat.at(2, 2) = y;
	mat.at(3, 3) = z;
	return mat;
}

inline Matrix4 scale3D(float scale) {
	return scale3D(scale, scale, scale);
}

inline Matrix4 scale3D(const Vector3& vec) {
	return scale3D(vec[1], vec[2], vec[3]);
}

inline Matrix4 rotateX(float theta) {
	Matrix4 mat = identity3D();
	mat.at(2, 2) = std::cos(theta);
	mat.at(2, 3) = -std::sin(theta);
	mat.at(3, 2) = std::sin(theta);
	mat.at(3, 3) = std::cos(theta);
	return mat;
}

inline Matrix4 rotateY(float theta) {
	Matrix4 mat = identity3D();
	mat.at(1, 1) = std::cos(theta);
	mat.at(1, 3) = std::sin(theta);
	mat.at(3, 1) = -std::sin(theta);
	mat.at(3, 3) = std::cos(theta);
	return mat;
}

inline Matrix4 rotateZ(float theta) {
	Matrix4 mat = identity3D();
	mat.at(1, 1) = std::cos(theta);
	mat.at(1, 2) = -std::sin(theta);
	mat.at(2, 1) = std::sin(theta);
	mat.at(2, 2) = std::cos(theta);
	return mat;
}

inline Matrix4 perspective3D(float fov_x, float fov_y, float near_z, float far_z) {
	Matrix4 mat = Matrix4::empty();
	mat.at(1, 1) = 1.0f / tanf(fov_x / 2.0f);
	mat.at(2, 2) = 1.0f / tanf(fov_y / 2.0f);
	mat.at(3, 3) = -(far_z + near_z) / (far_z - near_z);
	mat.at(3, 4) = -(2 * far_z * near_z) / (far_z - near_z);
	mat.at(4, 3) = -1.0f;
	mat.at(4, 4) = 0.0f;
	return mat;
}

inline Matrix4 ortho3D(float width, float height, float near_z, float far_z) {
	Matrix4 mat = identity3D();
	mat.at(1, 1) = 1.0f / width;
	mat.at(2, 2) = 1.0f / height;
	mat.at(3, 3) = -2.0f / (far_z - near_z);
	mat.at(3, 4) = -(far_z + near_z) / (far_z - near_z);
	return mat;
}

inline float dot_product(const Vector3 &a, const Vector3 &b) {
	return a[1]*b[1] + a[2]*b[2] + a[3]*b[3];
}

inline Vector3 cross_product(const Vector3 &a, const Vector3 &b) {
	return Vector3 {
		a[2]*b[3] - a[3]*b[2],
		a[3]*b[1] - a[1]*b[3],
		a[1]*b[2] - a[2]*b[1]
	};
}

inline Vector3 operator^(const Vector3 &a, const Vector3 &b) {
	return cross_product(a, b);
}

inline Matrix4 look_at(const Vector3 &eye, const Vector3 &target, const Vector3 &up) {
	Matrix4 mat = identity3D();

	Vector3 f = normalize(target - eye);
	Vector3 s = normalize(f ^ normalize(up));
	Vector3 u = s ^ f;

	mat.at(1, 1) = s.x();
	mat.at(1, 2) = s.y();
	mat.at(1, 3) = s.z();
	mat.at(2, 1) = u.x();
	mat.at(2, 2) = u.y();
	mat.at(2, 3) = u.z();
	mat.at(3, 1) =-f.x();
	mat.at(3, 2) =-f.y();
	mat.at(3, 3) =-f.z();
	mat.at(1, 4) =-dot_product(s, eye);
	mat.at(2, 4) =-dot_product(u, eye);
	mat.at(3, 4) = dot_product(f, eye);

	return mat;
}

#endif //TRANSFORM_3D_H
