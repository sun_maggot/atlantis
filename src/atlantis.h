#ifndef ATLANTIS_H
#define ATLANTIS_H

#define uint8 unsigned char
#define uint32 unsigned int 
#define array_size(array) (sizeof(array) / sizeof(array[0]))

#define TYPE(value)
#define SHADER(value)

#define TEST_STR_LIT "I'm a string \" literal"

SHADER()
struct PlainMaterial {
	float shininess;
	float specular_strength;
	Vector3 color;
};

struct StaticMesh {
	int vertex_count;

	size_t positions_offset;
	Vector3 *positions;

	size_t normals_offset;
	Vector3 *normals;

	size_t uv_offset;
	Vector2 *uv;

	int index_count;
	unsigned int *indices;

	void *plat_data;
};

void init_platform_data(void **mem, StaticMesh *mesh);
StaticMesh *allocate_static_mesh(void **mem, int vertex_count, int index_count);
StaticMesh *create_quad(void **mem, int width = 1, int height = 1);
StaticMesh *create_sphere(void **mem, int lat_count = 40, int lon_count = 20);
StaticMesh *create_box(void **mem, float width, float height, float depth);
StaticMesh *create_cube(void **mem, float size = 1.0f);
void calc_vertex_normals(StaticMesh *mesh);

#endif
