
#ifndef VECTOR_H
#define VECTOR_H

#include <cmath>

template <int Size>
struct Vector {
	static_assert(Size >= 1, "Size of vector must be at least 1");

	float v[Size];

	inline static Vector<Size> empty() {
		Vector<Size> result;
		for (int i = 1; i <= Size; i++) {
			result.at(i) = 0.0f;
		}
		return result;
	}

	inline float at(int i) const {
#ifdef DEBUG
		if ((i - 1) < 0 || (i - 1) >= Size) {
			abort();
		}
#endif
		return v[i - 1];
	}

	inline float &at(int i) {
#ifdef DEBUG
		if ((i - 1) < 0 || (i - 1) >= Size) {
			abort();
		}
#endif
		return v[i - 1];
	}

	inline float operator[](int i) const {
		return at(i);
	}

	inline float &operator[](int i) {
		return at(i);
	}

	inline float x() const {
		return at(1);
	}

	inline float &x() {
		return at(1);
	}

	inline float y() const {
		static_assert(Size >= 2, "Member y of vector require vector size at least 2");
		return at(2);
	}

	inline float &y() {
		static_assert(Size >= 2, "Member y of vector require vector size at least 2");
		return at(2);
	}

	inline float z() const {
		static_assert(Size >= 2, "Member z of vector require vector size at least 3");
		return at(3);
	}

	inline float &z() {
		static_assert(Size >= 2, "Member z of vector require vector size at least 3");
		return at(3);
	}

	inline Vector<3> xyz() const {
		static_assert(Size >= 3, "Member xyz of vector require vector size at least 3");
		return { at(1), at(2), at(3) };
	}

	Vector<Size> &operator+=(const Vector<Size> &b) {
		for (int i = 1; i <= Size; i++) {
			at(i) += b.at(i);
		}
		return *this;
	}

	Vector<Size> &operator-=(const Vector<Size> &b) {
		for (int i = 1; i <= Size; i++) {
			at(i) -= b.at(i);
		}
		return *this;
	}
};

template<int Size>
Vector<Size> multiply(const Vector<Size> &vec, float scalar) {
	Vector<Size> result;
	for (int i = 1; i <= Size; i++) {
		result.at(i) = scalar * vec.at(i);
	}
	return result;
}

template<int Size>
Vector<Size> operator*(const Vector<Size> &vec, float scalar) {
	return multiply(vec, scalar);
}

template<int Size>
Vector<Size> operator*(float scalar, const Vector<Size> &vec) {
	return multiply(vec, scalar);
}

template<int Size>
Vector<Size> operator+(const Vector<Size> &a, const Vector<Size> &b) {
	Vector<Size> result;
	for (int i = 1; i <= Size; i++) {
		result.at(i) = a.at(i) + b.at(i);
	}
	return result;
}

template<int Size>
Vector<Size> operator-(const Vector<Size> &a, const Vector<Size> &b) {
	Vector<Size> result;
	for (int i = 1; i <= Size; i++) {
		result.at(i) = a.at(i) - b.at(i);
	}
	return result;
}

bool is_zero(float num) {
	return num == 0.0f;
}

template<int Size>
float size_squared(const Vector<Size> &vec) {
	float result = 0.0f;
	for (int i = 1; i <= Size; i++) {
		result += vec.at(i) * vec.at(i);
	}
	return result;
}

template<int Size>
float size(const Vector<Size> &vec) {
	return std::sqrt(size_squared(vec));
}

template<int Size>
Vector<Size> normalize(const Vector<Size> &vec) {
	float mag_squared = size_squared(vec);
	if (is_zero(mag_squared)) {
		return Vector<Size>::empty();
	}
	return vec * (1.0f / std::sqrt(mag_squared));
}

template<int Size>
Vector<Size> operator-(const Vector<Size> vec) {
	Vector<Size> result;
	for (int i = 1; i <= Size; i++) {
		result[i] = -vec[i];
	}
	return result;
}


#endif
